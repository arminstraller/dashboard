/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * File: model_data.c
 *
 * Code generated for Simulink model 'model'.
 *
 * Model version                  : 1.325
 * Simulink Coder version         : 8.11 (R2016b) 25-Aug-2016
 * C/C++ source code generated on : Thu Jun 14 18:42:28 2018
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: ARM Compatible->ARM Cortex
 * Code generation objectives:
 *    1. Execution efficiency
 *    2. RAM efficiency
 * Validation result: Not run
 */

#include "model.h"

/* Invariant block signals (auto storage) */
const ConstB rtConstB = {
  7,                                   /* '<S22>/Subtract' */
  14,                                  /* '<S9>/Subtract' */
  13,                                  /* '<S12>/Subtract' */
  6,                                   /* '<S23>/Subtract' */
  4,                                   /* '<S7>/Subtract' */
  5,                                   /* '<S8>/Subtract' */
  0,                                   /* '<S19>/Subtract' */
  11,                                  /* '<S11>/Subtract' */
  4,                                   /* '<S20>/Subtract' */
  2,                                   /* '<S13>/Subtract' */
  15,                                  /* '<S17>/Subtract' */
  2,                                   /* '<S10>/Subtract' */
  12,                                  /* '<S15>/Subtract' */
  12,                                  /* '<S18>/Subtract' */
  11,                                  /* '<S14>/Subtract' */
  10,                                  /* '<S16>/Subtract' */
  1                                    /* '<S21>/Subtract' */
};

/* Constant parameters (auto storage) */
const ConstP rtConstP = {
  /* Expression: 4.12
   * Referenced by: '<Root>/Constant19'
   */
  4.12,

  /* Expression: 4.09
   * Referenced by: '<Root>/Constant20'
   */
  4.09,

  /* Expression: 35.0
   * Referenced by: '<Root>/Constant21'
   */
  35.0,

  /* Pooled Parameter (Expression: )
   * Referenced by:
   *   '<S7>/Constant'
   *   '<S7>/Constant2'
   *   '<S8>/Constant'
   *   '<S8>/Constant2'
   *   '<S9>/Constant2'
   *   '<S10>/Constant2'
   *   '<S11>/Constant'
   *   '<S11>/Constant2'
   *   '<S12>/Constant2'
   *   '<S13>/Constant2'
   *   '<S14>/Constant2'
   *   '<S15>/Constant'
   *   '<S15>/Constant2'
   *   '<S16>/Constant2'
   *   '<S17>/Constant'
   *   '<S17>/Constant2'
   *   '<S18>/Constant2'
   *   '<S19>/Constant1'
   *   '<S19>/Constant2'
   *   '<S20>/Constant2'
   *   '<S21>/Constant2'
   *   '<S22>/Constant'
   *   '<S22>/Constant2'
   *   '<S23>/Constant'
   *   '<S23>/Constant2'
   */
  1,

  /* Pooled Parameter (Expression: )
   * Referenced by:
   *   '<S9>/Constant'
   *   '<S10>/Constant1'
   *   '<S12>/Constant'
   *   '<S13>/Constant'
   *   '<S13>/Constant1'
   *   '<S14>/Constant'
   *   '<S16>/Constant'
   *   '<S18>/Constant'
   *   '<S19>/Constant'
   *   '<S20>/Constant'
   *   '<S21>/Constant'
   */
  3,

  /* Computed Parameter: Constant_Value_k
   * Referenced by: '<S10>/Constant'
   */
  4,

  /* Computed Parameter: Constant_Value_ku
   * Referenced by: '<S4>/Constant'
   */
  389U,

  /* Computed Parameter: Constant1_Value_p
   * Referenced by: '<S4>/Constant1'
   */
  4U,

  /* Pooled Parameter (Expression: )
   * Referenced by:
   *   '<S1>/Constant1'
   *   '<S2>/Constant1'
   *   '<S3>/Constant1'
   *   '<S4>/Constant2'
   */
  1U,

  /* Computed Parameter: Constant_Value_n
   * Referenced by: '<S3>/Constant'
   */
  400U,

  /* Computed Parameter: Constant_Value_ks
   * Referenced by: '<S1>/Constant'
   */
  391U,

  /* Computed Parameter: Constant18_Value
   * Referenced by: '<Root>/Constant18'
   */
  83U,

  /* Computed Parameter: Constant_Value_j
   * Referenced by: '<S2>/Constant'
   */
  390U,

  /* Pooled Parameter (Mixed Expressions)
   * Referenced by:
   *   '<Root>/Constant1'
   *   '<Root>/Constant12'
   *   '<Root>/Constant13'
   *   '<Root>/Constant2'
   *   '<Root>/Constant3'
   *   '<Root>/Constant4'
   *   '<Root>/Constant6'
   *   '<Root>/Constant7'
   *   '<S31>/ '
   *   '<S48>/Unit Delay'
   *   '<S51>/Unit Delay'
   *   '<S57>/Unit Delay'
   *   '<S49>/OUT'
   *   '<S50>/OUT'
   *   '<S52>/OUT'
   *   '<S53>/OUT'
   *   '<S55>/OUT'
   *   '<S56>/OUT'
   *   '<S58>/OUT'
   *   '<S59>/OUT'
   *   '<S61>/OUT'
   *   '<S62>/OUT'
   */
  0,

  /* Pooled Parameter (Mixed Expressions)
   * Referenced by:
   *   '<Root>/Constant'
   *   '<Root>/Constant10'
   *   '<Root>/Pulse Generator'
   *   '<Root>/Pulse Generator2'
   *   '<Root>/Pulse Generator3'
   *   '<Root>/Pulse Generator4'
   *   '<S54>/Unit Delay'
   *   '<S60>/Unit Delay'
   */
  1
};

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
