/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * File: model.c
 *
 * Code generated for Simulink model 'model'.
 *
 * Model version                  : 1.325
 * Simulink Coder version         : 8.11 (R2016b) 25-Aug-2016
 * C/C++ source code generated on : Thu Jun 14 18:42:28 2018
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: ARM Compatible->ARM Cortex
 * Code generation objectives:
 *    1. Execution efficiency
 *    2. RAM efficiency
 * Validation result: Not run
 */

#include "model.h"

/* Named constants for Chart: '<Root>/Chart' */
#define IN_COOLING                     ((uint8_T)1U)
#define IN_DRIVING                     ((uint8_T)2U)
#define IN_HV                          ((uint8_T)3U)
#define IN_IDLE                        ((uint8_T)4U)
#define IN_LV                          ((uint8_T)5U)
#define IN_RTD                         ((uint8_T)6U)
#define IN_SDC                         ((uint8_T)7U)

/* Named constants for Chart: '<S24>/Encoder States' */
#define IN_down                        ((uint8_T)1U)
#define IN_down1                       ((uint8_T)2U)
#define IN_down2                       ((uint8_T)3U)
#define IN_down3                       ((uint8_T)4U)
#define IN_state00                     ((uint8_T)5U)
#define IN_state01                     ((uint8_T)6U)
#define IN_state10                     ((uint8_T)7U)
#define IN_state11                     ((uint8_T)8U)
#define IN_up                          ((uint8_T)9U)
#define IN_up1                         ((uint8_T)10U)
#define IN_up2                         ((uint8_T)11U)
#define IN_up3                         ((uint8_T)12U)

const real_T model_RGND = 0.0;         /* real_T ground */
const uint32_T model_U32GND = 0U;      /* uint32_T ground */

/* Block signals and states (auto storage) */
DW rtDW;

/* Previous zero-crossings (trigger) states */
PrevZCX rtPrevZCX;

/* Real-time model */
RT_MODEL rtM_;
RT_MODEL *const rtM = &rtM_;
extern void gpio_in_Outputs_wrapper(const int32_T *port_popupvalue,
  const int32_T *pin_number,
  boolean_T *gpio_in,
  const real_T *xD);
extern void gpio_in_Update_wrapper(const int32_T *port_popupvalue,
  const int32_T *pin_number,
  boolean_T *gpio_in,
  real_T *xD);
extern void can_tx_Outputs_wrapper(const boolean_T *trigger,
  const uint32_T *id,
  const uint32_T *dlc,
  const uint8_T *data,
  const uint32_T *channel,
  const real_T *xD);
extern void can_tx_Update_wrapper(const boolean_T *trigger,
  const uint32_T *id,
  const uint32_T *dlc,
  const uint8_T *data,
  const uint32_T *channel,
  real_T *xD);
extern void can_rx_Outputs_wrapper(const uint32_T *id,
  const uint32_T *channel,
  boolean_T *received,
  uint8_T *data,
  const real_T *xD);
extern void can_rx_Update_wrapper(const uint32_T *id,
  const uint32_T *channel,
  boolean_T *received,
  uint8_T *data,
  real_T *xD);
extern void gpio_out_Outputs_wrapper(const int32_T *port_popupvalue,
  const int32_T *pin_number,
  const boolean_T *gpio_out,
  const real_T *xD);
extern void gpio_out_Update_wrapper(const int32_T *port_popupvalue,
  const int32_T *pin_number,
  const boolean_T *gpio_out,
  real_T *xD);
extern void oled_idle_Outputs_wrapper(const boolean_T *show,
  const real_T *xD);
extern void oled_idle_Update_wrapper(const boolean_T *show,
  real_T *xD);
extern void oled_rtd_Outputs_wrapper(const boolean_T *show,
  const real_T *xD);
extern void oled_rtd_Update_wrapper(const boolean_T *show,
  real_T *xD);
extern void oled_driving_Outputs_wrapper(const real_T *power,
  const uint32_T *hv_soc,
  const uint32_T *lv_soc,
  const uint32_T *error,
  const real_T *cell_min,
  const uint32_T *accu_temp,
  const boolean_T *show,
  const real_T *xD);
extern void oled_driving_Update_wrapper(const real_T *power,
  const uint32_T *hv_soc,
  const uint32_T *lv_soc,
  const uint32_T *error,
  const real_T *cell_min,
  const uint32_T *accu_temp,
  const boolean_T *show,
  real_T *xD);
extern void oled_lv_Outputs_wrapper(const real_T *lv_voltage,
  const uint32_T *lv_soc,
  const boolean_T *show,
  const real_T *xD);
extern void oled_lv_Update_wrapper(const real_T *lv_voltage,
  const uint32_T *lv_soc,
  const boolean_T *show,
  real_T *xD);
extern void oled_hv_Outputs_wrapper(const uint32_T *hv_soc,
  const real_T *cell_max,
  const real_T *cell_min,
  const real_T *temp_max,
  const real_T *temp_min,
  const boolean_T *show,
  const real_T *xD);
extern void oled_hv_Update_wrapper(const uint32_T *hv_soc,
  const real_T *cell_max,
  const real_T *cell_min,
  const real_T *temp_max,
  const real_T *temp_min,
  const boolean_T *show,
  real_T *xD);
extern void oled_sdc_Outputs_wrapper(const uint32_T *sdc_status,
  const boolean_T *show,
  const real_T *xD);
extern void oled_sdc_Update_wrapper(const uint32_T *sdc_status,
  const boolean_T *show,
  real_T *xD);
extern void oled_cooling_Outputs_wrapper(const uint32_T *fan,
  const real_T *temp_max,
  const boolean_T *show,
  const real_T *xD);
extern void oled_cooling_Update_wrapper(const uint32_T *fan,
  const real_T *temp_max,
  const boolean_T *show,
  real_T *xD);
extern void oled_draw_Outputs_wrapper(const boolean_T *draw,
  const real_T *xD);
extern void oled_draw_Update_wrapper(const boolean_T *draw,
  real_T *xD);
extern void NEGATIVEEdge(real_T rtu_Enable, boolean_T rtu_IN, boolean_T
  rtu_INprevious, boolean_T *rty_OUT);
extern void POSITIVEEdge(real_T rtu_Enable, boolean_T rtu_IN, boolean_T
  rtu_INprevious, boolean_T *rty_OUT);
static void rate_scheduler(void);

/*
 *   This function updates active task flag for each subrate.
 * The function is called at model base rate, hence the
 * generated code self-manages all its subrates.
 */
static void rate_scheduler(void)
{
  /* Compute which subrates run during the next base time step.  Subrates
   * are an integer multiple of the base rate counter.  Therefore, the subtask
   * counter is reset when it reaches its limit (zero means run).
   */
  (rtM->Timing.TaskCounters.TID[1])++;
  if ((rtM->Timing.TaskCounters.TID[1]) > 999) {/* Sample time: [1.0s, 0.0s] */
    rtM->Timing.TaskCounters.TID[1] = 0;
  }
}

/*
 * Output and update for enable system:
 *    '<S48>/NEGATIVE Edge'
 *    '<S51>/NEGATIVE Edge'
 *    '<S54>/NEGATIVE Edge'
 *    '<S57>/NEGATIVE Edge'
 *    '<S60>/NEGATIVE Edge'
 */
void NEGATIVEEdge(real_T rtu_Enable, boolean_T rtu_IN, boolean_T rtu_INprevious,
                  boolean_T *rty_OUT)
{
  /* Outputs for Enabled SubSystem: '<S48>/NEGATIVE Edge' incorporates:
   *  EnablePort: '<S49>/Enable'
   */
  if (rtu_Enable > 0.0) {
    /* RelationalOperator: '<S49>/Relational Operator1' */
    *rty_OUT = ((int32_T)rtu_INprevious > (int32_T)rtu_IN);
  }

  /* End of Outputs for SubSystem: '<S48>/NEGATIVE Edge' */
}

/*
 * Output and update for enable system:
 *    '<S48>/POSITIVE Edge'
 *    '<S51>/POSITIVE Edge'
 *    '<S54>/POSITIVE Edge'
 *    '<S57>/POSITIVE Edge'
 *    '<S60>/POSITIVE Edge'
 */
void POSITIVEEdge(real_T rtu_Enable, boolean_T rtu_IN, boolean_T rtu_INprevious,
                  boolean_T *rty_OUT)
{
  /* Outputs for Enabled SubSystem: '<S48>/POSITIVE Edge' incorporates:
   *  EnablePort: '<S50>/Enable'
   */
  if (rtu_Enable > 0.0) {
    /* RelationalOperator: '<S50>/Relational Operator1' */
    *rty_OUT = ((int32_T)rtu_IN > (int32_T)rtu_INprevious);
  }

  /* End of Outputs for SubSystem: '<S48>/POSITIVE Edge' */
}

/* Model step function */
void model_step(void)
{
  int32_T bitIdx;
  int32_T i;
  uint32_T intVal;
  uint32_T bitMask;
  boolean_T rtb_PulseGenerator2;
  real_T rtb_Gain;
  boolean_T rtb_LogicalOperator1_b;
  boolean_T rtb_LogicalOperator1_l;
  boolean_T rtb_UnitDelay_j;

  /* DiscretePulseGenerator: '<Root>/Pulse Generator' */
  rtDW.PulseGenerator = ((rtDW.clockTickCounter < 1) && (rtDW.clockTickCounter >=
    0));
  if (rtDW.clockTickCounter >= 99) {
    rtDW.clockTickCounter = 0;
  } else {
    rtDW.clockTickCounter++;
  }

  /* End of DiscretePulseGenerator: '<Root>/Pulse Generator' */

  /* S-Function (gpio_in): '<S22>/S-Function Builder' */
  gpio_in_Outputs_wrapper(&rtConstP.pooled11, &rtConstB.Subtract,
    &rtDW.SFunctionBuilder, &rtDW.SFunctionBuilder_DSTATE);

  /* S-Function (scanpack): '<Root>/CAN Pack2' */
  rtDW.CANPack2.ID = 389U;
  rtDW.CANPack2.Length = 4U;
  rtDW.CANPack2.Extended = 0U;
  rtDW.CANPack2.Remote = 0;
  rtDW.CANPack2.Data[0] = 0;
  rtDW.CANPack2.Data[1] = 0;
  rtDW.CANPack2.Data[2] = 0;
  rtDW.CANPack2.Data[3] = 0;
  rtDW.CANPack2.Data[4] = 0;
  rtDW.CANPack2.Data[5] = 0;
  rtDW.CANPack2.Data[6] = 0;
  rtDW.CANPack2.Data[7] = 0;

  {
    /* --------------- START Packing signal 0 ------------------
     *  startBit                = 0
     *  length                  = 1
     *  desiredSignalByteLayout = LITTLEENDIAN
     *  dataType                = UNSIGNED
     *  factor                  = 1.0
     *  offset                  = 0.0
     *  minimum                 = 0.0
     *  maximum                 = 0.0
     * -----------------------------------------------------------------------*/
    {
      {
        uint32_T packingValue = 0;

        {
          uint32_T result = (uint32_T) (rtDW.SFunctionBuilder);

          /* no scaling required */
          packingValue = result;
        }

        {
          {
            uint8_T packedValue;
            if (packingValue > (boolean_T)(1)) {
              packedValue = (uint8_T) 1;
            } else if (packingValue < (boolean_T)(0)) {
              packedValue = (uint8_T) 0;
            } else {
              packedValue = (uint8_T) (packingValue);
            }

            {
              {
                rtDW.CANPack2.Data[0] = rtDW.CANPack2.Data[0] | (uint8_T)
                  ((uint8_T)((uint8_T)(packedValue & (uint8_T)(((uint8_T)(1)) <<
                      0)) >> 0)<<0);
              }
            }
          }
        }
      }
    }
  }

  /* S-Function (scanunpack): '<Root>/CAN Unpack2' */
  {
    /* S-Function (scanunpack): '<Root>/CAN Unpack2' */
    if ((4 == rtDW.CANPack2.Length) && (rtDW.CANPack2.ID != INVALID_CAN_ID) ) {
      if ((389U == rtDW.CANPack2.ID) && (0U == rtDW.CANPack2.Extended) ) {
        (void) memcpy(&rtDW.MatrixConcatenate2[0], rtDW.CANPack2.Data,
                      4 * sizeof(uint8_T));
      }
    }
  }

  /* SignalConversion: '<Root>/ConcatBufferAtMatrix Concatenate2In2' */
  rtDW.MatrixConcatenate2[4] = 0U;
  rtDW.MatrixConcatenate2[5] = 0U;
  rtDW.MatrixConcatenate2[6] = 0U;
  rtDW.MatrixConcatenate2[7] = 0U;

  /* S-Function (can_tx): '<S4>/can_tx' */
  can_tx_Outputs_wrapper(&rtDW.PulseGenerator, &rtConstP.Constant_Value_ku,
    &rtConstP.Constant1_Value_p, &rtDW.MatrixConcatenate2[0], &rtConstP.pooled16,
    &rtDW.can_tx_DSTATE);

  /* S-Function (can_rx): '<S3>/S-Function Builder' */
  can_rx_Outputs_wrapper(&rtConstP.Constant_Value_n, &rtConstP.pooled16,
    &rtDW.SFunctionBuilder_o1, &rtDW.SFunctionBuilder_o2[0],
    &rtDW.SFunctionBuilder_DSTATE_j);

  /* S-Function (scanpack): '<Root>/CAN Pack1' */
  rtDW.CANPack1.ID = 400U;
  rtDW.CANPack1.Length = 8U;
  rtDW.CANPack1.Extended = 0U;
  rtDW.CANPack1.Remote = 0;
  rtDW.CANPack1.Data[0] = 0;
  rtDW.CANPack1.Data[1] = 0;
  rtDW.CANPack1.Data[2] = 0;
  rtDW.CANPack1.Data[3] = 0;
  rtDW.CANPack1.Data[4] = 0;
  rtDW.CANPack1.Data[5] = 0;
  rtDW.CANPack1.Data[6] = 0;
  rtDW.CANPack1.Data[7] = 0;

  {
    (void) memcpy((rtDW.CANPack1.Data), &rtDW.SFunctionBuilder_o2[0],
                  8 * sizeof(uint8_T));
  }

  /* S-Function (scanunpack): '<Root>/CAN Unpack1' */
  {
    /* S-Function (scanunpack): '<Root>/CAN Unpack1' */
    if ((8 == rtDW.CANPack1.Length) && (rtDW.CANPack1.ID != INVALID_CAN_ID) ) {
      if ((400U == rtDW.CANPack1.ID) && (0U == rtDW.CANPack1.Extended) ) {
        {
          /* --------------- START Unpacking signal 0 ------------------
           *  startBit                = 4
           *  length                  = 1
           *  desiredSignalByteLayout = LITTLEENDIAN
           *  dataType                = UNSIGNED
           *  factor                  = 1.0
           *  offset                  = 0.0
           * -----------------------------------------------------------------------*/
          {
            {
              real64_T outValue = 0;

              {
                {
                  uint8_T unpackedValue = 0;

                  {
                    uint8_T tempValue = (uint8_T) (0);

                    {
                      tempValue = tempValue | (uint8_T)((uint8_T)((uint8_T)
                        ((rtDW.CANPack1.Data[0]) & (uint8_T)( (uint8_T) (1)<< 4))
                        >> 4)<<0);
                    }

                    unpackedValue = tempValue;
                  }

                  outValue = (real64_T) (unpackedValue);
                }
              }

              {
                real64_T result = (real64_T) outValue;
                rtDW.CANUnpack1_o1 = result;
              }
            }
          }

          /* --------------- START Unpacking signal 1 ------------------
           *  startBit                = 0
           *  length                  = 1
           *  desiredSignalByteLayout = LITTLEENDIAN
           *  dataType                = UNSIGNED
           *  factor                  = 1.0
           *  offset                  = 0.0
           * -----------------------------------------------------------------------*/
          /*
           * Signal is not connected or connected to terminator.
           * No unpacking code generated.
           */

          /* --------------- START Unpacking signal 2 ------------------
           *  startBit                = 8
           *  length                  = 8
           *  desiredSignalByteLayout = LITTLEENDIAN
           *  dataType                = UNSIGNED
           *  factor                  = 1.0
           *  offset                  = 0.0
           * -----------------------------------------------------------------------*/
          /*
           * Signal is not connected or connected to terminator.
           * No unpacking code generated.
           */

          /* --------------- START Unpacking signal 3 ------------------
           *  startBit                = 3
           *  length                  = 1
           *  desiredSignalByteLayout = LITTLEENDIAN
           *  dataType                = UNSIGNED
           *  factor                  = 1.0
           *  offset                  = 0.0
           * -----------------------------------------------------------------------*/
          /*
           * Signal is not connected or connected to terminator.
           * No unpacking code generated.
           */

          /* --------------- START Unpacking signal 4 ------------------
           *  startBit                = 5
           *  length                  = 1
           *  desiredSignalByteLayout = LITTLEENDIAN
           *  dataType                = UNSIGNED
           *  factor                  = 1.0
           *  offset                  = 0.0
           * -----------------------------------------------------------------------*/
          {
            {
              real64_T outValue = 0;

              {
                {
                  uint8_T unpackedValue = 0;

                  {
                    uint8_T tempValue = (uint8_T) (0);

                    {
                      tempValue = tempValue | (uint8_T)((uint8_T)((uint8_T)
                        ((rtDW.CANPack1.Data[0]) & (uint8_T)( (uint8_T) (1)<< 5))
                        >> 5)<<0);
                    }

                    unpackedValue = tempValue;
                  }

                  outValue = (real64_T) (unpackedValue);
                }
              }

              {
                real64_T result = (real64_T) outValue;
                rtDW.CANUnpack1_o5 = result;
              }
            }
          }

          /* --------------- START Unpacking signal 5 ------------------
           *  startBit                = 2
           *  length                  = 1
           *  desiredSignalByteLayout = LITTLEENDIAN
           *  dataType                = UNSIGNED
           *  factor                  = 1.0
           *  offset                  = 0.0
           * -----------------------------------------------------------------------*/
          {
            {
              uint8_T outValue = 0;

              {
                {
                  uint8_T unpackedValue = 0;

                  {
                    uint8_T tempValue = (uint8_T) (0);

                    {
                      tempValue = tempValue | (uint8_T)((uint8_T)((uint8_T)
                        ((rtDW.CANPack1.Data[0]) & (uint8_T)( (uint8_T) (1)<< 2))
                        >> 2)<<0);
                    }

                    unpackedValue = tempValue;
                  }

                  outValue = (boolean_T) (unpackedValue);
                }
              }

              {
                boolean_T result = (boolean_T) outValue;
                rtDW.CANUnpack1_o6 = result;
              }
            }
          }

          /* --------------- START Unpacking signal 6 ------------------
           *  startBit                = 1
           *  length                  = 1
           *  desiredSignalByteLayout = LITTLEENDIAN
           *  dataType                = UNSIGNED
           *  factor                  = 1.0
           *  offset                  = 0.0
           * -----------------------------------------------------------------------*/
          {
            {
              real64_T outValue = 0;

              {
                {
                  uint8_T unpackedValue = 0;

                  {
                    uint8_T tempValue = (uint8_T) (0);

                    {
                      tempValue = tempValue | (uint8_T)((uint8_T)((uint8_T)
                        ((rtDW.CANPack1.Data[0]) & (uint8_T)( (uint8_T) (1)<< 1))
                        >> 1)<<0);
                    }

                    unpackedValue = tempValue;
                  }

                  outValue = (real64_T) (unpackedValue);
                }
              }

              {
                real64_T result = (real64_T) outValue;
                rtDW.CANUnpack1_o7 = result;
              }
            }
          }
        }
      }
    }
  }

  /* Outputs for Triggered SubSystem: '<Root>/Sample and Hold2' incorporates:
   *  TriggerPort: '<S30>/Trigger'
   */
  if (rtDW.SFunctionBuilder_o1 && (rtPrevZCX.SampleandHold2_Trig_ZCE !=
       POS_ZCSIG)) {
    /* Inport: '<S30>/In' */
    rtDW.In_f = rtDW.CANUnpack1_o1;
  }

  rtPrevZCX.SampleandHold2_Trig_ZCE = rtDW.SFunctionBuilder_o1;

  /* End of Outputs for SubSystem: '<Root>/Sample and Hold2' */
  if (rtM->Timing.TaskCounters.TID[1] == 0) {
    /* Step: '<S38>/Step2' */
    rtDW.Step2 = (((rtM->Timing.clockTick1) ) < 2.0);
  }

  /* DiscreteIntegrator: '<S6>/Discrete-Time Integrator' */
  if (rtDW.SFunctionBuilder_o1 && (rtDW.DiscreteTimeIntegrator_PrevRese <= 0)) {
    rtDW.DiscreteTimeIntegrator_DSTATE = 0.0;
  }

  /* RelationalOperator: '<S41>/Compare' incorporates:
   *  Constant: '<S41>/Constant'
   *  DiscreteIntegrator: '<S6>/Discrete-Time Integrator'
   */
  rtb_PulseGenerator2 = (rtDW.DiscreteTimeIntegrator_DSTATE >= 3.0);

  /* Logic: '<S38>/Logical Operator3' */
  rtDW.LogicalOperator3 = ((rtDW.In_f != 0.0) || (rtDW.Step2 != 0.0) ||
    rtb_PulseGenerator2);

  /* S-Function (gpio_out): '<S9>/S-Function Builder' */
  gpio_out_Outputs_wrapper(&rtConstP.pooled12, &rtConstB.Subtract_e,
    &rtDW.LogicalOperator3, &rtDW.SFunctionBuilder_DSTATE_j2);

  /* Outputs for Triggered SubSystem: '<Root>/Sample and Hold1' incorporates:
   *  TriggerPort: '<S25>/Trigger'
   */
  if (rtDW.SFunctionBuilder_o1 && (rtPrevZCX.SampleandHold1_Trig_ZCE !=
       POS_ZCSIG)) {
    /* Inport: '<S25>/In' */
    rtDW.In_i = rtDW.CANUnpack1_o5;
  }

  rtPrevZCX.SampleandHold1_Trig_ZCE = rtDW.SFunctionBuilder_o1;

  /* End of Outputs for SubSystem: '<Root>/Sample and Hold1' */
  if (rtM->Timing.TaskCounters.TID[1] == 0) {
    /* Step: '<S39>/Step2' */
    rtDW.Step2_p = (((rtM->Timing.clockTick1) ) < 2.0);
  }

  /* Logic: '<S39>/Logical Operator3' */
  rtDW.LogicalOperator3_o = ((rtDW.In_i != 0.0) || (rtDW.Step2_p != 0.0) ||
    rtb_PulseGenerator2);

  /* S-Function (gpio_out): '<S12>/S-Function Builder' */
  gpio_out_Outputs_wrapper(&rtConstP.pooled12, &rtConstB.Subtract_ep,
    &rtDW.LogicalOperator3_o, &rtDW.SFunctionBuilder_DSTATE_jo);

  /* Outputs for Triggered SubSystem: '<Root>/Sample and Hold3' incorporates:
   *  TriggerPort: '<S31>/Trigger'
   */
  if (rtDW.SFunctionBuilder_o1 && (rtPrevZCX.SampleandHold3_Trig_ZCE !=
       POS_ZCSIG)) {
    /* Inport: '<S31>/In' */
    rtDW.In_j = rtDW.CANUnpack1_o6;
  }

  rtPrevZCX.SampleandHold3_Trig_ZCE = rtDW.SFunctionBuilder_o1;

  /* End of Outputs for SubSystem: '<Root>/Sample and Hold3' */

  /* S-Function (gpio_out): '<S23>/S-Function Builder' */
  gpio_out_Outputs_wrapper(&rtConstP.pooled11, &rtConstB.Subtract_b, &rtDW.In_j,
    &rtDW.SFunctionBuilder_DSTATE_d);

  /* Gain: '<Root>/Gain' incorporates:
   *  DiscreteIntegrator: '<Root>/Integrator Limited'
   */
  rtb_Gain = 1000.0 * rtDW.IntegratorLimited_DSTATE;

  /* Chart: '<Root>/Chart' */
  if (rtDW.temporalCounter_i1 < 2047U) {
    rtDW.temporalCounter_i1++;
  }

  /* Gateway: Chart */
  /* During: Chart */
  if (rtDW.is_active_c1_model == 0U) {
    /* Entry: Chart */
    rtDW.is_active_c1_model = 1U;

    /* Entry Internal: Chart */
    /* Transition: '<S5>:2' */
    rtDW.is_c1_model = IN_IDLE;
    rtDW.temporalCounter_i1 = 0U;

    /* Entry 'IDLE': '<S5>:1' */
    rtDW.idle = true;
    rtDW.rtd = false;
    rtDW.driving = false;
    rtDW.lv = false;
    rtDW.hv = false;
    rtDW.sdc = false;
    rtDW.cooling = false;
  } else {
    switch (rtDW.is_c1_model) {
     case IN_COOLING:
      /* During 'COOLING': '<S5>:18' */
      if (rtDW.CANUnpack1_o7 == 1.0) {
        /* Transition: '<S5>:28' */
        rtDW.is_c1_model = IN_RTD;
        rtDW.temporalCounter_i1 = 0U;

        /* Entry 'RTD': '<S5>:20' */
        rtDW.idle = false;
        rtDW.rtd = true;
        rtDW.driving = false;
        rtDW.lv = false;
        rtDW.hv = false;
        rtDW.sdc = false;
        rtDW.cooling = false;
      } else {
        if (rtb_Gain < 3.0) {
          /* Transition: '<S5>:33' */
          rtDW.is_c1_model = IN_SDC;

          /* Entry 'SDC': '<S5>:16' */
          rtDW.idle = false;
          rtDW.rtd = false;
          rtDW.driving = false;
          rtDW.lv = false;
          rtDW.hv = false;
          rtDW.sdc = true;
          rtDW.cooling = false;
        }
      }
      break;

     case IN_DRIVING:
      /* During 'DRIVING': '<S5>:21' */
      if (rtDW.CANUnpack1_o7 == 0.0) {
        /* Transition: '<S5>:23' */
        rtDW.is_c1_model = IN_LV;

        /* Entry 'LV': '<S5>:11' */
        rtDW.idle = false;
        rtDW.rtd = false;
        rtDW.driving = false;
        rtDW.lv = true;
        rtDW.hv = false;
        rtDW.sdc = false;
        rtDW.cooling = false;
      }
      break;

     case IN_HV:
      /* During 'HV': '<S5>:14' */
      if (rtb_Gain >= 2.0) {
        /* Transition: '<S5>:17' */
        rtDW.is_c1_model = IN_SDC;

        /* Entry 'SDC': '<S5>:16' */
        rtDW.idle = false;
        rtDW.rtd = false;
        rtDW.driving = false;
        rtDW.lv = false;
        rtDW.hv = false;
        rtDW.sdc = true;
        rtDW.cooling = false;
      } else if (rtDW.CANUnpack1_o7 == 1.0) {
        /* Transition: '<S5>:26' */
        rtDW.is_c1_model = IN_RTD;
        rtDW.temporalCounter_i1 = 0U;

        /* Entry 'RTD': '<S5>:20' */
        rtDW.idle = false;
        rtDW.rtd = true;
        rtDW.driving = false;
        rtDW.lv = false;
        rtDW.hv = false;
        rtDW.sdc = false;
        rtDW.cooling = false;
      } else {
        if (rtb_Gain < 1.0) {
          /* Transition: '<S5>:31' */
          rtDW.is_c1_model = IN_LV;

          /* Entry 'LV': '<S5>:11' */
          rtDW.idle = false;
          rtDW.rtd = false;
          rtDW.driving = false;
          rtDW.lv = true;
          rtDW.hv = false;
          rtDW.sdc = false;
          rtDW.cooling = false;
        }
      }
      break;

     case IN_IDLE:
      /* During 'IDLE': '<S5>:1' */
      if ((rtDW.temporalCounter_i1 >= 2000U) && (rtb_Gain >= 0.0)) {
        /* Transition: '<S5>:12' */
        rtDW.is_c1_model = IN_LV;

        /* Entry 'LV': '<S5>:11' */
        rtDW.idle = false;
        rtDW.rtd = false;
        rtDW.driving = false;
        rtDW.lv = true;
        rtDW.hv = false;
        rtDW.sdc = false;
        rtDW.cooling = false;
      }
      break;

     case IN_LV:
      /* During 'LV': '<S5>:11' */
      if (rtb_Gain >= 1.0) {
        /* Transition: '<S5>:15' */
        rtDW.is_c1_model = IN_HV;

        /* Entry 'HV': '<S5>:14' */
        rtDW.idle = false;
        rtDW.rtd = false;
        rtDW.driving = false;
        rtDW.lv = false;
        rtDW.hv = true;
        rtDW.sdc = false;
        rtDW.cooling = false;
      } else {
        if (rtDW.CANUnpack1_o7 == 1.0) {
          /* Transition: '<S5>:25' */
          rtDW.is_c1_model = IN_RTD;
          rtDW.temporalCounter_i1 = 0U;

          /* Entry 'RTD': '<S5>:20' */
          rtDW.idle = false;
          rtDW.rtd = true;
          rtDW.driving = false;
          rtDW.lv = false;
          rtDW.hv = false;
          rtDW.sdc = false;
          rtDW.cooling = false;
        }
      }
      break;

     case IN_RTD:
      /* During 'RTD': '<S5>:20' */
      if (rtDW.temporalCounter_i1 >= 2000U) {
        /* Transition: '<S5>:22' */
        rtDW.is_c1_model = IN_DRIVING;

        /* Entry 'DRIVING': '<S5>:21' */
        rtDW.idle = false;
        rtDW.rtd = false;
        rtDW.driving = false;
        rtDW.lv = false;
        rtDW.hv = false;
        rtDW.sdc = false;
        rtDW.cooling = true;
      }
      break;

     default:
      /* During 'SDC': '<S5>:16' */
      if (rtb_Gain >= 3.0) {
        /* Transition: '<S5>:19' */
        rtDW.is_c1_model = IN_COOLING;

        /* Entry 'COOLING': '<S5>:18' */
        rtDW.idle = false;
        rtDW.rtd = false;
        rtDW.driving = false;
        rtDW.lv = false;
        rtDW.hv = false;
        rtDW.sdc = false;
        rtDW.cooling = true;
      } else if (rtDW.CANUnpack1_o7 == 1.0) {
        /* Transition: '<S5>:27' */
        rtDW.is_c1_model = IN_RTD;
        rtDW.temporalCounter_i1 = 0U;

        /* Entry 'RTD': '<S5>:20' */
        rtDW.idle = false;
        rtDW.rtd = true;
        rtDW.driving = false;
        rtDW.lv = false;
        rtDW.hv = false;
        rtDW.sdc = false;
        rtDW.cooling = false;
      } else {
        if (rtb_Gain < 2.0) {
          /* Transition: '<S5>:32' */
          rtDW.is_c1_model = IN_HV;

          /* Entry 'HV': '<S5>:14' */
          rtDW.idle = false;
          rtDW.rtd = false;
          rtDW.driving = false;
          rtDW.lv = false;
          rtDW.hv = true;
          rtDW.sdc = false;
          rtDW.cooling = false;
        }
      }
      break;
    }
  }

  /* End of Chart: '<Root>/Chart' */

  /* S-Function (oled_idle): '<Root>/S-Function Builder' */
  oled_idle_Outputs_wrapper(&rtDW.idle, &rtDW.SFunctionBuilder_DSTATE_o);

  /* S-Function (oled_rtd): '<Root>/S-Function Builder1' */
  oled_rtd_Outputs_wrapper(&rtDW.rtd, &rtDW.SFunctionBuilder1_DSTATE);

  /* S-Function (oled_driving): '<Root>/S-Function Builder2' */
  oled_driving_Outputs_wrapper((real_T*)&model_RGND, (uint32_T*)&model_U32GND,
    (uint32_T*)&model_U32GND, (uint32_T*)&model_U32GND, (real_T*)&model_RGND,
    (uint32_T*)&model_U32GND, &rtDW.driving, &rtDW.SFunctionBuilder2_DSTATE);

  /* S-Function (can_rx): '<S1>/S-Function Builder' */
  can_rx_Outputs_wrapper(&rtConstP.Constant_Value_ks, &rtConstP.pooled16,
    &rtDW.SFunctionBuilder_o1_a, &rtDW.SFunctionBuilder_o2_f[0],
    &rtDW.SFunctionBuilder_DSTATE_jo1);

  /* S-Function (scanpack): '<Root>/CAN Pack3' */
  rtDW.CANPack3.ID = 391U;
  rtDW.CANPack3.Length = 8U;
  rtDW.CANPack3.Extended = 0U;
  rtDW.CANPack3.Remote = 0;
  rtDW.CANPack3.Data[0] = 0;
  rtDW.CANPack3.Data[1] = 0;
  rtDW.CANPack3.Data[2] = 0;
  rtDW.CANPack3.Data[3] = 0;
  rtDW.CANPack3.Data[4] = 0;
  rtDW.CANPack3.Data[5] = 0;
  rtDW.CANPack3.Data[6] = 0;
  rtDW.CANPack3.Data[7] = 0;

  {
    (void) memcpy((rtDW.CANPack3.Data), &rtDW.SFunctionBuilder_o2_f[0],
                  8 * sizeof(uint8_T));
  }

  /* S-Function (scanunpack): '<Root>/CAN Unpack3' */
  {
    /* S-Function (scanunpack): '<Root>/CAN Unpack3' */
    if ((8 == rtDW.CANPack3.Length) && (rtDW.CANPack3.ID != INVALID_CAN_ID) ) {
      if ((391U == rtDW.CANPack3.ID) && (0U == rtDW.CANPack3.Extended) ) {
        {
          /* --------------- START Unpacking signal 0 ------------------
           *  startBit                = 24
           *  length                  = 12
           *  desiredSignalByteLayout = LITTLEENDIAN
           *  dataType                = UNSIGNED
           *  factor                  = 1.0
           *  offset                  = 0.0
           * -----------------------------------------------------------------------*/
          /*
           * Signal is not connected or connected to terminator.
           * No unpacking code generated.
           */

          /* --------------- START Unpacking signal 1 ------------------
           *  startBit                = 0
           *  length                  = 12
           *  desiredSignalByteLayout = LITTLEENDIAN
           *  dataType                = UNSIGNED
           *  factor                  = 1.0
           *  offset                  = 0.0
           * -----------------------------------------------------------------------*/
          /*
           * Signal is not connected or connected to terminator.
           * No unpacking code generated.
           */

          /* --------------- START Unpacking signal 2 ------------------
           *  startBit                = 48
           *  length                  = 15
           *  desiredSignalByteLayout = LITTLEENDIAN
           *  dataType                = UNSIGNED
           *  factor                  = 1.0
           *  offset                  = 0.0
           * -----------------------------------------------------------------------*/
          {
            {
              real64_T outValue = 0;

              {
                {
                  uint16_T unpackedValue = 0;

                  {
                    uint16_T tempValue = (uint16_T) (0);

                    {
                      tempValue = tempValue | (uint16_T)((uint16_T)((uint16_T)
                        ((rtDW.CANPack3.Data[6]) & (uint16_T)( (uint16_T) (1)<<
                        0)) >> 0)<<0);
                    }

                    {
                      tempValue = tempValue | (uint16_T)((uint16_T)((uint16_T)
                        ((rtDW.CANPack3.Data[6]) & (uint16_T)( (uint16_T) (1)<<
                        1)) >> 1)<<1);
                    }

                    {
                      tempValue = tempValue | (uint16_T)((uint16_T)((uint16_T)
                        ((rtDW.CANPack3.Data[6]) & (uint16_T)( (uint16_T) (1)<<
                        2)) >> 2)<<2);
                    }

                    {
                      tempValue = tempValue | (uint16_T)((uint16_T)((uint16_T)
                        ((rtDW.CANPack3.Data[6]) & (uint16_T)( (uint16_T) (1)<<
                        3)) >> 3)<<3);
                    }

                    {
                      tempValue = tempValue | (uint16_T)((uint16_T)((uint16_T)
                        ((rtDW.CANPack3.Data[6]) & (uint16_T)( (uint16_T) (1)<<
                        4)) >> 4)<<4);
                    }

                    {
                      tempValue = tempValue | (uint16_T)((uint16_T)((uint16_T)
                        ((rtDW.CANPack3.Data[6]) & (uint16_T)( (uint16_T) (1)<<
                        5)) >> 5)<<5);
                    }

                    {
                      tempValue = tempValue | (uint16_T)((uint16_T)((uint16_T)
                        ((rtDW.CANPack3.Data[6]) & (uint16_T)( (uint16_T) (1)<<
                        6)) >> 6)<<6);
                    }

                    {
                      tempValue = tempValue | (uint16_T)((uint16_T)((uint16_T)
                        ((rtDW.CANPack3.Data[6]) & (uint16_T)( (uint16_T) (1)<<
                        7)) >> 7)<<7);
                    }

                    {
                      tempValue = tempValue | (uint16_T)((uint16_T)((uint16_T)
                        ((rtDW.CANPack3.Data[7]) & (uint16_T)( (uint16_T) (1)<<
                        0)) >> 0)<<8);
                    }

                    {
                      tempValue = tempValue | (uint16_T)((uint16_T)((uint16_T)
                        ((rtDW.CANPack3.Data[7]) & (uint16_T)( (uint16_T) (1)<<
                        1)) >> 1)<<9);
                    }

                    {
                      tempValue = tempValue | (uint16_T)((uint16_T)((uint16_T)
                        ((rtDW.CANPack3.Data[7]) & (uint16_T)( (uint16_T) (1)<<
                        2)) >> 2)<<10);
                    }

                    {
                      tempValue = tempValue | (uint16_T)((uint16_T)((uint16_T)
                        ((rtDW.CANPack3.Data[7]) & (uint16_T)( (uint16_T) (1)<<
                        3)) >> 3)<<11);
                    }

                    {
                      tempValue = tempValue | (uint16_T)((uint16_T)((uint16_T)
                        ((rtDW.CANPack3.Data[7]) & (uint16_T)( (uint16_T) (1)<<
                        4)) >> 4)<<12);
                    }

                    {
                      tempValue = tempValue | (uint16_T)((uint16_T)((uint16_T)
                        ((rtDW.CANPack3.Data[7]) & (uint16_T)( (uint16_T) (1)<<
                        5)) >> 5)<<13);
                    }

                    {
                      tempValue = tempValue | (uint16_T)((uint16_T)((uint16_T)
                        ((rtDW.CANPack3.Data[7]) & (uint16_T)( (uint16_T) (1)<<
                        6)) >> 6)<<14);
                    }

                    unpackedValue = tempValue;
                  }

                  outValue = (real64_T) (unpackedValue);
                }
              }

              {
                real64_T result = (real64_T) outValue;
                rtDW.CANUnpack3_o3 = result;
              }
            }
          }

          /* --------------- START Unpacking signal 3 ------------------
           *  startBit                = 23
           *  length                  = 1
           *  desiredSignalByteLayout = LITTLEENDIAN
           *  dataType                = UNSIGNED
           *  factor                  = 1.0
           *  offset                  = 0.0
           * -----------------------------------------------------------------------*/
          {
            {
              real64_T outValue = 0;

              {
                {
                  uint8_T unpackedValue = 0;

                  {
                    uint8_T tempValue = (uint8_T) (0);

                    {
                      tempValue = tempValue | (uint8_T)((uint8_T)((uint8_T)
                        ((rtDW.CANPack3.Data[2]) & (uint8_T)( (uint8_T) (1)<< 7))
                        >> 7)<<0);
                    }

                    unpackedValue = tempValue;
                  }

                  outValue = (real64_T) (unpackedValue);
                }
              }

              {
                real64_T result = (real64_T) outValue;
                rtDW.CANUnpack3_o4 = result;
              }
            }
          }

          /* --------------- START Unpacking signal 4 ------------------
           *  startBit                = 12
           *  length                  = 1
           *  desiredSignalByteLayout = LITTLEENDIAN
           *  dataType                = UNSIGNED
           *  factor                  = 1.0
           *  offset                  = 0.0
           * -----------------------------------------------------------------------*/
          {
            {
              real64_T outValue = 0;

              {
                {
                  uint8_T unpackedValue = 0;

                  {
                    uint8_T tempValue = (uint8_T) (0);

                    {
                      tempValue = tempValue | (uint8_T)((uint8_T)((uint8_T)
                        ((rtDW.CANPack3.Data[1]) & (uint8_T)( (uint8_T) (1)<< 4))
                        >> 4)<<0);
                    }

                    unpackedValue = tempValue;
                  }

                  outValue = (real64_T) (unpackedValue);
                }
              }

              {
                real64_T result = (real64_T) outValue;
                rtDW.CANUnpack3_o5 = result;
              }
            }
          }

          /* --------------- START Unpacking signal 5 ------------------
           *  startBit                = 21
           *  length                  = 1
           *  desiredSignalByteLayout = LITTLEENDIAN
           *  dataType                = UNSIGNED
           *  factor                  = 1.0
           *  offset                  = 0.0
           * -----------------------------------------------------------------------*/
          {
            {
              real64_T outValue = 0;

              {
                {
                  uint8_T unpackedValue = 0;

                  {
                    uint8_T tempValue = (uint8_T) (0);

                    {
                      tempValue = tempValue | (uint8_T)((uint8_T)((uint8_T)
                        ((rtDW.CANPack3.Data[2]) & (uint8_T)( (uint8_T) (1)<< 5))
                        >> 5)<<0);
                    }

                    unpackedValue = tempValue;
                  }

                  outValue = (real64_T) (unpackedValue);
                }
              }

              {
                real64_T result = (real64_T) outValue;
                rtDW.CANUnpack3_o6 = result;
              }
            }
          }

          /* --------------- START Unpacking signal 6 ------------------
           *  startBit                = 14
           *  length                  = 1
           *  desiredSignalByteLayout = LITTLEENDIAN
           *  dataType                = UNSIGNED
           *  factor                  = 1.0
           *  offset                  = 0.0
           * -----------------------------------------------------------------------*/
          {
            {
              real64_T outValue = 0;

              {
                {
                  uint8_T unpackedValue = 0;

                  {
                    uint8_T tempValue = (uint8_T) (0);

                    {
                      tempValue = tempValue | (uint8_T)((uint8_T)((uint8_T)
                        ((rtDW.CANPack3.Data[1]) & (uint8_T)( (uint8_T) (1)<< 6))
                        >> 6)<<0);
                    }

                    unpackedValue = tempValue;
                  }

                  outValue = (real64_T) (unpackedValue);
                }
              }

              {
                real64_T result = (real64_T) outValue;
                rtDW.CANUnpack3_o7 = result;
              }
            }
          }

          /* --------------- START Unpacking signal 7 ------------------
           *  startBit                = 44
           *  length                  = 4
           *  desiredSignalByteLayout = LITTLEENDIAN
           *  dataType                = UNSIGNED
           *  factor                  = 1.0
           *  offset                  = 0.0
           * -----------------------------------------------------------------------*/
          /*
           * Signal is not connected or connected to terminator.
           * No unpacking code generated.
           */
        }
      }
    }
  }

  /* Outputs for Triggered SubSystem: '<Root>/Sample and Hold4' incorporates:
   *  TriggerPort: '<S32>/Trigger'
   */
  if (rtDW.SFunctionBuilder_o1_a && (rtPrevZCX.SampleandHold4_Trig_ZCE !=
       POS_ZCSIG)) {
    /* Inport: '<S32>/In' */
    rtDW.In = rtDW.CANUnpack3_o3;
  }

  rtPrevZCX.SampleandHold4_Trig_ZCE = rtDW.SFunctionBuilder_o1_a;

  /* End of Outputs for SubSystem: '<Root>/Sample and Hold4' */

  /* Gain: '<Root>/Gain2' */
  rtDW.Gain2 = 0.001 * rtDW.In;

  /* Saturate: '<Root>/Saturation' */
  if (rtDW.Gain2 > 28.8) {
    rtb_Gain = 28.8;
  } else if (rtDW.Gain2 < 22.4) {
    rtb_Gain = 22.4;
  } else {
    rtb_Gain = rtDW.Gain2;
  }

  /* End of Saturate: '<Root>/Saturation' */

  /* Gain: '<Root>/Gain3' incorporates:
   *  Constant: '<Root>/Constant5'
   *  Sum: '<Root>/Sum'
   */
  rtDW.Gain3 = (uint32_T)((rtb_Gain - 22.4) * 15.625);

  /* S-Function (oled_lv): '<Root>/S-Function Builder3' */
  oled_lv_Outputs_wrapper(&rtDW.Gain2, &rtDW.Gain3, &rtDW.lv,
    &rtDW.SFunctionBuilder3_DSTATE);

  /* S-Function (oled_hv): '<Root>/S-Function Builder4' */
  oled_hv_Outputs_wrapper(&rtConstP.Constant18_Value, &rtConstP.Constant19_Value,
    &rtConstP.Constant20_Value, &rtConstP.Constant21_Value,
    &rtConstP.Constant21_Value, &rtDW.hv, &rtDW.SFunctionBuilder4_DSTATE);

  /* Outputs for Triggered SubSystem: '<Root>/Sample and Hold6' incorporates:
   *  TriggerPort: '<S34>/Trigger'
   */
  if (rtDW.SFunctionBuilder_o1_a && (rtPrevZCX.SampleandHold6_Trig_ZCE !=
       POS_ZCSIG)) {
    /* Inport: '<S34>/In' */
    rtDW.VectorConcatenate[0] = rtDW.CANUnpack3_o5;
  }

  rtPrevZCX.SampleandHold6_Trig_ZCE = rtDW.SFunctionBuilder_o1_a;

  /* End of Outputs for SubSystem: '<Root>/Sample and Hold6' */

  /* Outputs for Triggered SubSystem: '<Root>/Sample and Hold8' incorporates:
   *  TriggerPort: '<S36>/Trigger'
   */
  if (rtDW.SFunctionBuilder_o1_a && (rtPrevZCX.SampleandHold8_Trig_ZCE !=
       POS_ZCSIG)) {
    /* Inport: '<S36>/In' */
    rtDW.VectorConcatenate[1] = rtDW.CANUnpack3_o7;
  }

  rtPrevZCX.SampleandHold8_Trig_ZCE = rtDW.SFunctionBuilder_o1_a;

  /* End of Outputs for SubSystem: '<Root>/Sample and Hold8' */

  /* Outputs for Triggered SubSystem: '<Root>/Sample and Hold7' incorporates:
   *  TriggerPort: '<S35>/Trigger'
   */
  if (rtDW.SFunctionBuilder_o1_a && (rtPrevZCX.SampleandHold7_Trig_ZCE !=
       POS_ZCSIG)) {
    /* Inport: '<S35>/In' */
    rtDW.VectorConcatenate[2] = rtDW.CANUnpack3_o6;
  }

  rtPrevZCX.SampleandHold7_Trig_ZCE = rtDW.SFunctionBuilder_o1_a;

  /* End of Outputs for SubSystem: '<Root>/Sample and Hold7' */

  /* Outputs for Triggered SubSystem: '<Root>/Sample and Hold5' incorporates:
   *  TriggerPort: '<S33>/Trigger'
   */
  if (rtDW.SFunctionBuilder_o1_a && (rtPrevZCX.SampleandHold5_Trig_ZCE !=
       POS_ZCSIG)) {
    /* Inport: '<S33>/In' */
    rtDW.VectorConcatenate[3] = rtDW.CANUnpack3_o4;
  }

  rtPrevZCX.SampleandHold5_Trig_ZCE = rtDW.SFunctionBuilder_o1_a;

  /* End of Outputs for SubSystem: '<Root>/Sample and Hold5' */

  /* S-Function (can_rx): '<S2>/S-Function Builder' */
  can_rx_Outputs_wrapper(&rtConstP.Constant_Value_j, &rtConstP.pooled16,
    &rtDW.SFunctionBuilder_o1_c, &rtDW.SFunctionBuilder_o2_a[0],
    &rtDW.SFunctionBuilder_DSTATE_h);

  /* S-Function (scanpack): '<Root>/CAN Pack4' */
  rtDW.CANPack4.ID = 390U;
  rtDW.CANPack4.Length = 6U;
  rtDW.CANPack4.Extended = 0U;
  rtDW.CANPack4.Remote = 0;
  rtDW.CANPack4.Data[0] = 0;
  rtDW.CANPack4.Data[1] = 0;
  rtDW.CANPack4.Data[2] = 0;
  rtDW.CANPack4.Data[3] = 0;
  rtDW.CANPack4.Data[4] = 0;
  rtDW.CANPack4.Data[5] = 0;
  rtDW.CANPack4.Data[6] = 0;
  rtDW.CANPack4.Data[7] = 0;

  {
    (void) memcpy((rtDW.CANPack4.Data), &rtDW.SFunctionBuilder_o2_a[0],
                  6 * sizeof(uint8_T));
  }

  /* S-Function (scanunpack): '<Root>/CAN Unpack4' */
  {
    /* S-Function (scanunpack): '<Root>/CAN Unpack4' */
    if ((6 == rtDW.CANPack4.Length) && (rtDW.CANPack4.ID != INVALID_CAN_ID) ) {
      if ((390U == rtDW.CANPack4.ID) && (0U == rtDW.CANPack4.Extended) ) {
        {
          /* --------------- START Unpacking signal 0 ------------------
           *  startBit                = 16
           *  length                  = 12
           *  desiredSignalByteLayout = LITTLEENDIAN
           *  dataType                = UNSIGNED
           *  factor                  = 1.0
           *  offset                  = 0.0
           * -----------------------------------------------------------------------*/
          /*
           * Signal is not connected or connected to terminator.
           * No unpacking code generated.
           */

          /* --------------- START Unpacking signal 1 ------------------
           *  startBit                = 32
           *  length                  = 12
           *  desiredSignalByteLayout = LITTLEENDIAN
           *  dataType                = UNSIGNED
           *  factor                  = 1.0
           *  offset                  = 0.0
           * -----------------------------------------------------------------------*/
          /*
           * Signal is not connected or connected to terminator.
           * No unpacking code generated.
           */

          /* --------------- START Unpacking signal 2 ------------------
           *  startBit                = 0
           *  length                  = 7
           *  desiredSignalByteLayout = LITTLEENDIAN
           *  dataType                = UNSIGNED
           *  factor                  = 1.0
           *  offset                  = 0.0
           * -----------------------------------------------------------------------*/
          /*
           * Signal is not connected or connected to terminator.
           * No unpacking code generated.
           */

          /* --------------- START Unpacking signal 3 ------------------
           *  startBit                = 8
           *  length                  = 7
           *  desiredSignalByteLayout = LITTLEENDIAN
           *  dataType                = UNSIGNED
           *  factor                  = 1.0
           *  offset                  = 0.0
           * -----------------------------------------------------------------------*/
          /*
           * Signal is not connected or connected to terminator.
           * No unpacking code generated.
           */

          /* --------------- START Unpacking signal 4 ------------------
           *  startBit                = 15
           *  length                  = 1
           *  desiredSignalByteLayout = LITTLEENDIAN
           *  dataType                = UNSIGNED
           *  factor                  = 1.0
           *  offset                  = 0.0
           * -----------------------------------------------------------------------*/
          {
            {
              real64_T outValue = 0;

              {
                {
                  uint8_T unpackedValue = 0;

                  {
                    uint8_T tempValue = (uint8_T) (0);

                    {
                      tempValue = tempValue | (uint8_T)((uint8_T)((uint8_T)
                        ((rtDW.CANPack4.Data[1]) & (uint8_T)( (uint8_T) (1)<< 7))
                        >> 7)<<0);
                    }

                    unpackedValue = tempValue;
                  }

                  outValue = (real64_T) (unpackedValue);
                }
              }

              {
                real64_T result = (real64_T) outValue;
                rtDW.CANUnpack4_o5 = result;
              }
            }
          }

          /* --------------- START Unpacking signal 5 ------------------
           *  startBit                = 44
           *  length                  = 1
           *  desiredSignalByteLayout = LITTLEENDIAN
           *  dataType                = UNSIGNED
           *  factor                  = 1.0
           *  offset                  = 0.0
           * -----------------------------------------------------------------------*/
          {
            {
              real64_T outValue = 0;

              {
                {
                  uint8_T unpackedValue = 0;

                  {
                    uint8_T tempValue = (uint8_T) (0);

                    {
                      tempValue = tempValue | (uint8_T)((uint8_T)((uint8_T)
                        ((rtDW.CANPack4.Data[5]) & (uint8_T)( (uint8_T) (1)<< 4))
                        >> 4)<<0);
                    }

                    unpackedValue = tempValue;
                  }

                  outValue = (real64_T) (unpackedValue);
                }
              }

              {
                real64_T result = (real64_T) outValue;
                rtDW.CANUnpack4_o6 = result;
              }
            }
          }

          /* --------------- START Unpacking signal 6 ------------------
           *  startBit                = 45
           *  length                  = 1
           *  desiredSignalByteLayout = LITTLEENDIAN
           *  dataType                = UNSIGNED
           *  factor                  = 1.0
           *  offset                  = 0.0
           * -----------------------------------------------------------------------*/
          {
            {
              real64_T outValue = 0;

              {
                {
                  uint8_T unpackedValue = 0;

                  {
                    uint8_T tempValue = (uint8_T) (0);

                    {
                      tempValue = tempValue | (uint8_T)((uint8_T)((uint8_T)
                        ((rtDW.CANPack4.Data[5]) & (uint8_T)( (uint8_T) (1)<< 5))
                        >> 5)<<0);
                    }

                    unpackedValue = tempValue;
                  }

                  outValue = (real64_T) (unpackedValue);
                }
              }

              {
                real64_T result = (real64_T) outValue;
                rtDW.CANUnpack4_o7 = result;
              }
            }
          }

          /* --------------- START Unpacking signal 7 ------------------
           *  startBit                = 46
           *  length                  = 1
           *  desiredSignalByteLayout = LITTLEENDIAN
           *  dataType                = UNSIGNED
           *  factor                  = 1.0
           *  offset                  = 0.0
           * -----------------------------------------------------------------------*/
          {
            {
              real64_T outValue = 0;

              {
                {
                  uint8_T unpackedValue = 0;

                  {
                    uint8_T tempValue = (uint8_T) (0);

                    {
                      tempValue = tempValue | (uint8_T)((uint8_T)((uint8_T)
                        ((rtDW.CANPack4.Data[5]) & (uint8_T)( (uint8_T) (1)<< 6))
                        >> 6)<<0);
                    }

                    unpackedValue = tempValue;
                  }

                  outValue = (real64_T) (unpackedValue);
                }
              }

              {
                real64_T result = (real64_T) outValue;
                rtDW.CANUnpack4_o8 = result;
              }
            }
          }

          /* --------------- START Unpacking signal 8 ------------------
           *  startBit                = 47
           *  length                  = 1
           *  desiredSignalByteLayout = LITTLEENDIAN
           *  dataType                = UNSIGNED
           *  factor                  = 1.0
           *  offset                  = 0.0
           * -----------------------------------------------------------------------*/
          {
            {
              real64_T outValue = 0;

              {
                {
                  uint8_T unpackedValue = 0;

                  {
                    uint8_T tempValue = (uint8_T) (0);

                    {
                      tempValue = tempValue | (uint8_T)((uint8_T)((uint8_T)
                        ((rtDW.CANPack4.Data[5]) & (uint8_T)( (uint8_T) (1)<< 7))
                        >> 7)<<0);
                    }

                    unpackedValue = tempValue;
                  }

                  outValue = (real64_T) (unpackedValue);
                }
              }

              {
                real64_T result = (real64_T) outValue;
                rtDW.CANUnpack4_o9 = result;
              }
            }
          }

          /* --------------- START Unpacking signal 9 ------------------
           *  startBit                = 28
           *  length                  = 4
           *  desiredSignalByteLayout = LITTLEENDIAN
           *  dataType                = UNSIGNED
           *  factor                  = 1.0
           *  offset                  = 0.0
           * -----------------------------------------------------------------------*/
          /*
           * Signal is not connected or connected to terminator.
           * No unpacking code generated.
           */
        }
      }
    }
  }

  /* Outputs for Triggered SubSystem: '<Root>/Sample and Hold13' incorporates:
   *  TriggerPort: '<S29>/Trigger'
   */
  if (rtDW.SFunctionBuilder_o1_c && (rtPrevZCX.SampleandHold13_Trig_ZCE !=
       POS_ZCSIG)) {
    /* Inport: '<S29>/In' */
    rtDW.VectorConcatenate[4] = rtDW.CANUnpack4_o9;
  }

  rtPrevZCX.SampleandHold13_Trig_ZCE = rtDW.SFunctionBuilder_o1_c;

  /* End of Outputs for SubSystem: '<Root>/Sample and Hold13' */

  /* Outputs for Triggered SubSystem: '<Root>/Sample and Hold9' incorporates:
   *  TriggerPort: '<S37>/Trigger'
   */
  if (rtDW.SFunctionBuilder_o1_c && (rtPrevZCX.SampleandHold9_Trig_ZCE !=
       POS_ZCSIG)) {
    /* Inport: '<S37>/In' */
    rtDW.VectorConcatenate[5] = rtDW.CANUnpack4_o5;
  }

  rtPrevZCX.SampleandHold9_Trig_ZCE = rtDW.SFunctionBuilder_o1_c;

  /* End of Outputs for SubSystem: '<Root>/Sample and Hold9' */

  /* Outputs for Triggered SubSystem: '<Root>/Sample and Hold11' incorporates:
   *  TriggerPort: '<S27>/Trigger'
   */
  if (rtDW.SFunctionBuilder_o1_c && (rtPrevZCX.SampleandHold11_Trig_ZCE !=
       POS_ZCSIG)) {
    /* Inport: '<S27>/In' */
    rtDW.VectorConcatenate[6] = rtDW.CANUnpack4_o7;
  }

  rtPrevZCX.SampleandHold11_Trig_ZCE = rtDW.SFunctionBuilder_o1_c;

  /* End of Outputs for SubSystem: '<Root>/Sample and Hold11' */

  /* Outputs for Triggered SubSystem: '<Root>/Sample and Hold10' incorporates:
   *  TriggerPort: '<S26>/Trigger'
   */
  if (rtDW.SFunctionBuilder_o1_c && (rtPrevZCX.SampleandHold10_Trig_ZCE !=
       POS_ZCSIG)) {
    /* Inport: '<S26>/In' */
    rtDW.VectorConcatenate[7] = rtDW.CANUnpack4_o6;
  }

  rtPrevZCX.SampleandHold10_Trig_ZCE = rtDW.SFunctionBuilder_o1_c;

  /* End of Outputs for SubSystem: '<Root>/Sample and Hold10' */

  /* Outputs for Triggered SubSystem: '<Root>/Sample and Hold12' incorporates:
   *  TriggerPort: '<S28>/Trigger'
   */
  if (rtDW.SFunctionBuilder_o1_c && (rtPrevZCX.SampleandHold12_Trig_ZCE !=
       POS_ZCSIG)) {
    /* Inport: '<S28>/In' */
    rtDW.VectorConcatenate[8] = rtDW.CANUnpack4_o8;
  }

  rtPrevZCX.SampleandHold12_Trig_ZCE = rtDW.SFunctionBuilder_o1_c;

  /* End of Outputs for SubSystem: '<Root>/Sample and Hold12' */

  /* S-Function (scominttobit): '<Root>/Bit to Integer Converter' */
  /* Bit to Integer Conversion */
  bitIdx = 0;
  i = 0;
  while (i < 1) {
    intVal = 0U;
    bitMask = 1U;
    for (i = 0; i < 9; i++) {
      /* Input bit order is LSB first */
      if ((uint32_T)rtDW.VectorConcatenate[bitIdx] != 0U) {
        intVal |= bitMask;
      }

      bitIdx++;
      bitMask <<= 1U;
    }

    rtDW.BittoIntegerConverter = intVal;
    i = 1;
  }

  /* End of S-Function (scominttobit): '<Root>/Bit to Integer Converter' */

  /* S-Function (oled_sdc): '<Root>/S-Function Builder5' */
  oled_sdc_Outputs_wrapper(&rtDW.BittoIntegerConverter, &rtDW.sdc,
    &rtDW.SFunctionBuilder5_DSTATE);

  /* S-Function (oled_cooling): '<Root>/S-Function Builder6' */
  oled_cooling_Outputs_wrapper((uint32_T*)&model_U32GND, (real_T*)&model_RGND,
    &rtDW.cooling, &rtDW.SFunctionBuilder6_DSTATE);

  /* S-Function (gpio_in): '<S7>/S-Function Builder' */
  gpio_in_Outputs_wrapper(&rtConstP.pooled11, &rtConstB.Subtract_c,
    &rtDW.SFunctionBuilder_e, &rtDW.SFunctionBuilder_DSTATE_ob);

  /* S-Function (gpio_in): '<S8>/S-Function Builder' */
  gpio_in_Outputs_wrapper(&rtConstP.pooled11, &rtConstB.Subtract_h,
    &rtDW.SFunctionBuilder_g, &rtDW.SFunctionBuilder_DSTATE_c);

  /* UnitDelay: '<S48>/Unit Delay' */
  rtb_PulseGenerator2 = rtDW.UnitDelay_DSTATE;

  /* Outputs for Enabled SubSystem: '<S48>/POSITIVE Edge' */
  POSITIVEEdge(1.0, rtDW.SFunctionBuilder_e, rtb_PulseGenerator2,
               &rtDW.RelationalOperator1_n3);

  /* End of Outputs for SubSystem: '<S48>/POSITIVE Edge' */

  /* Outputs for Enabled SubSystem: '<S48>/NEGATIVE Edge' */
  NEGATIVEEdge(0.0, rtDW.SFunctionBuilder_e, rtb_PulseGenerator2,
               &rtDW.RelationalOperator1_i);

  /* End of Outputs for SubSystem: '<S48>/NEGATIVE Edge' */

  /* Logic: '<S48>/Logical Operator1' */
  rtb_PulseGenerator2 = (rtDW.RelationalOperator1_n3 ||
    rtDW.RelationalOperator1_i);

  /* Outputs for Enabled SubSystem: '<S54>/POSITIVE Edge' */

  /* UnitDelay: '<S54>/Unit Delay' */
  POSITIVEEdge(0.0, rtDW.SFunctionBuilder_e, rtDW.UnitDelay_DSTATE_h,
               &rtDW.RelationalOperator1_a);

  /* End of Outputs for SubSystem: '<S54>/POSITIVE Edge' */

  /* Outputs for Enabled SubSystem: '<S54>/NEGATIVE Edge' */
  NEGATIVEEdge(1.0, rtDW.SFunctionBuilder_e, rtDW.UnitDelay_DSTATE_h,
               &rtDW.RelationalOperator1_l);

  /* End of Outputs for SubSystem: '<S54>/NEGATIVE Edge' */

  /* Logic: '<S54>/Logical Operator1' */
  rtb_LogicalOperator1_b = (rtDW.RelationalOperator1_a ||
    rtDW.RelationalOperator1_l);

  /* UnitDelay: '<S57>/Unit Delay' */
  rtb_UnitDelay_j = rtDW.UnitDelay_DSTATE_i;

  /* Outputs for Enabled SubSystem: '<S57>/POSITIVE Edge' */
  POSITIVEEdge(1.0, rtDW.SFunctionBuilder_g, rtb_UnitDelay_j,
               &rtDW.RelationalOperator1_m);

  /* End of Outputs for SubSystem: '<S57>/POSITIVE Edge' */

  /* Outputs for Enabled SubSystem: '<S57>/NEGATIVE Edge' */
  NEGATIVEEdge(0.0, rtDW.SFunctionBuilder_g, rtb_UnitDelay_j,
               &rtDW.RelationalOperator1_h);

  /* End of Outputs for SubSystem: '<S57>/NEGATIVE Edge' */

  /* Logic: '<S57>/Logical Operator1' */
  rtb_UnitDelay_j = (rtDW.RelationalOperator1_m || rtDW.RelationalOperator1_h);

  /* Outputs for Enabled SubSystem: '<S60>/POSITIVE Edge' */

  /* UnitDelay: '<S60>/Unit Delay' */
  POSITIVEEdge(0.0, rtDW.SFunctionBuilder_g, rtDW.UnitDelay_DSTATE_o,
               &rtDW.RelationalOperator1);

  /* End of Outputs for SubSystem: '<S60>/POSITIVE Edge' */

  /* Outputs for Enabled SubSystem: '<S60>/NEGATIVE Edge' */
  NEGATIVEEdge(1.0, rtDW.SFunctionBuilder_g, rtDW.UnitDelay_DSTATE_o,
               &rtDW.RelationalOperator1_g);

  /* End of Outputs for SubSystem: '<S60>/NEGATIVE Edge' */

  /* Logic: '<S60>/Logical Operator1' */
  rtb_LogicalOperator1_l = (rtDW.RelationalOperator1 ||
    rtDW.RelationalOperator1_g);

  /* Chart: '<S24>/Encoder States' */
  /* Gateway: Rotary Encoder Evaluation/Encoder States */
  /* During: Rotary Encoder Evaluation/Encoder States */
  if (rtDW.is_active_c3_model == 0U) {
    /* Entry: Rotary Encoder Evaluation/Encoder States */
    rtDW.is_active_c3_model = 1U;

    /* Entry Internal: Rotary Encoder Evaluation/Encoder States */
    /* Transition: '<S47>:6' */
    rtDW.is_c3_model = IN_state00;

    /* Entry 'state00': '<S47>:1' */
    rtDW.QCLK = 0.0;
  } else {
    switch (rtDW.is_c3_model) {
     case IN_down:
      /* During 'down': '<S47>:43' */
      /* Transition: '<S47>:44' */
      rtDW.is_c3_model = IN_state01;

      /* Entry 'state01': '<S47>:4' */
      rtDW.QCLK = 1.0;
      break;

     case IN_down1:
      /* During 'down1': '<S47>:45' */
      /* Transition: '<S47>:46' */
      rtDW.is_c3_model = IN_state11;

      /* Entry 'state11': '<S47>:5' */
      rtDW.QCLK = 0.0;
      break;

     case IN_down2:
      /* During 'down2': '<S47>:41' */
      /* Transition: '<S47>:42' */
      rtDW.is_c3_model = IN_state10;

      /* Entry 'state10': '<S47>:3' */
      rtDW.QCLK = 1.0;
      break;

     case IN_down3:
      /* During 'down3': '<S47>:47' */
      /* Transition: '<S47>:48' */
      rtDW.is_c3_model = IN_state00;

      /* Entry 'state00': '<S47>:1' */
      rtDW.QCLK = 0.0;
      break;

     case IN_state00:
      /* During 'state00': '<S47>:1' */
      if (rtb_PulseGenerator2 && (!rtDW.SFunctionBuilder_g)) {
        /* Transition: '<S47>:19' */
        rtDW.is_c3_model = IN_down;

        /* Entry 'down': '<S47>:43' */
        rtDW.QDIR = -1.0;
      } else {
        if (rtb_UnitDelay_j && (!rtDW.SFunctionBuilder_e)) {
          /* Transition: '<S47>:33' */
          rtDW.is_c3_model = IN_up;

          /* Entry 'up': '<S47>:32' */
          rtDW.QDIR = 1.0;
        }
      }
      break;

     case IN_state01:
      /* During 'state01': '<S47>:4' */
      if (rtb_LogicalOperator1_b && (!rtDW.SFunctionBuilder_g)) {
        /* Transition: '<S47>:16' */
        rtDW.is_c3_model = IN_up3;

        /* Entry 'up3': '<S47>:38' */
        rtDW.QDIR = 1.0;
      } else {
        if (rtb_UnitDelay_j && rtDW.SFunctionBuilder_e) {
          /* Transition: '<S47>:20' */
          rtDW.is_c3_model = IN_down1;

          /* Entry 'down1': '<S47>:45' */
          rtDW.QDIR = -1.0;
        }
      }
      break;

     case IN_state10:
      /* During 'state10': '<S47>:3' */
      if (rtb_LogicalOperator1_l && (!rtDW.SFunctionBuilder_e)) {
        /* Transition: '<S47>:13' */
        rtDW.is_c3_model = IN_down3;

        /* Entry 'down3': '<S47>:47' */
        rtDW.QDIR = -1.0;
      } else {
        if (rtb_PulseGenerator2 && rtDW.SFunctionBuilder_g) {
          /* Transition: '<S47>:14' */
          rtDW.is_c3_model = IN_up1;

          /* Entry 'up1': '<S47>:34' */
          rtDW.QDIR = 1.0;
        }
      }
      break;

     case IN_state11:
      /* During 'state11': '<S47>:5' */
      if (rtb_LogicalOperator1_l && rtDW.SFunctionBuilder_e) {
        /* Transition: '<S47>:15' */
        rtDW.is_c3_model = IN_up2;

        /* Entry 'up2': '<S47>:36' */
        rtDW.QDIR = 1.0;
      } else {
        if (rtb_LogicalOperator1_b && rtDW.SFunctionBuilder_g) {
          /* Transition: '<S47>:21' */
          rtDW.is_c3_model = IN_down2;

          /* Entry 'down2': '<S47>:41' */
          rtDW.QDIR = -1.0;
        }
      }
      break;

     case IN_up:
      /* During 'up': '<S47>:32' */
      /* Transition: '<S47>:40' */
      rtDW.is_c3_model = IN_state10;

      /* Entry 'state10': '<S47>:3' */
      rtDW.QCLK = 1.0;
      break;

     case IN_up1:
      /* During 'up1': '<S47>:34' */
      /* Transition: '<S47>:52' */
      rtDW.is_c3_model = IN_state11;

      /* Entry 'state11': '<S47>:5' */
      rtDW.QCLK = 0.0;
      break;

     case IN_up2:
      /* During 'up2': '<S47>:36' */
      /* Transition: '<S47>:37' */
      rtDW.is_c3_model = IN_state01;

      /* Entry 'state01': '<S47>:4' */
      rtDW.QCLK = 1.0;
      break;

     default:
      /* During 'up3': '<S47>:38' */
      /* Transition: '<S47>:39' */
      rtDW.is_c3_model = IN_state00;

      /* Entry 'state00': '<S47>:1' */
      rtDW.QCLK = 0.0;
      break;
    }
  }

  /* End of Chart: '<S24>/Encoder States' */

  /* Outputs for Enabled SubSystem: '<S51>/POSITIVE Edge' */

  /* DataTypeConversion: '<S51>/Data Type Conversion2' incorporates:
   *  UnitDelay: '<S51>/Unit Delay'
   */
  POSITIVEEdge(1.0, rtDW.QCLK != 0.0, rtDW.UnitDelay_DSTATE_ho,
               &rtDW.RelationalOperator1_n);

  /* End of Outputs for SubSystem: '<S51>/POSITIVE Edge' */

  /* Outputs for Enabled SubSystem: '<S51>/NEGATIVE Edge' */
  NEGATIVEEdge(0.0, rtDW.QCLK != 0.0, rtDW.UnitDelay_DSTATE_ho,
               &rtDW.RelationalOperator1_e);

  /* End of Outputs for SubSystem: '<S51>/NEGATIVE Edge' */

  /* S-Function (gpio_out): '<S19>/S-Function Builder' */
  gpio_out_Outputs_wrapper(&rtConstP.pooled12, &rtConstB.Subtract_k,
    &rtConstP.pooled18, &rtDW.SFunctionBuilder_DSTATE_m);

  /* S-Function (gpio_out): '<S11>/S-Function Builder' */
  gpio_out_Outputs_wrapper(&rtConstP.pooled11, &rtConstB.Subtract_g,
    &rtConstP.pooled17, &rtDW.SFunctionBuilder_DSTATE_i);

  /* S-Function (gpio_out): '<S20>/S-Function Builder' */
  gpio_out_Outputs_wrapper(&rtConstP.pooled12, &rtConstB.Subtract_i,
    &rtConstP.pooled18, &rtDW.SFunctionBuilder_DSTATE_oe);

  /* S-Function (gpio_out): '<S13>/S-Function Builder' */
  gpio_out_Outputs_wrapper(&rtConstP.pooled12, &rtConstB.Subtract_f,
    &rtConstP.pooled17, &rtDW.SFunctionBuilder_DSTATE_dx);

  /* S-Function (gpio_out): '<S17>/S-Function Builder' */
  gpio_out_Outputs_wrapper(&rtConstP.pooled11, &rtConstB.Subtract_go,
    &rtConstP.pooled17, &rtDW.SFunctionBuilder_DSTATE_e);

  /* S-Function (gpio_out): '<S10>/S-Function Builder' */
  gpio_out_Outputs_wrapper(&rtConstP.Constant_Value_k, &rtConstB.Subtract_n,
    &rtConstP.pooled17, &rtDW.SFunctionBuilder_DSTATE_a);

  /* S-Function (gpio_out): '<S15>/S-Function Builder' */
  gpio_out_Outputs_wrapper(&rtConstP.pooled11, &rtConstB.Subtract_fg,
    &rtConstP.pooled17, &rtDW.SFunctionBuilder_DSTATE_g);

  /* S-Function (gpio_out): '<S18>/S-Function Builder' */
  gpio_out_Outputs_wrapper(&rtConstP.pooled12, &rtConstB.Subtract_l,
    &rtConstP.pooled17, &rtDW.SFunctionBuilder_DSTATE_p);

  /* S-Function (gpio_out): '<S14>/S-Function Builder' */
  gpio_out_Outputs_wrapper(&rtConstP.pooled12, &rtConstB.Subtract_hi,
    &rtConstP.pooled17, &rtDW.SFunctionBuilder_DSTATE_n);

  /* S-Function (gpio_out): '<S16>/S-Function Builder' */
  gpio_out_Outputs_wrapper(&rtConstP.pooled12, &rtConstB.Subtract_gi,
    &rtConstP.pooled17, &rtDW.SFunctionBuilder_DSTATE_hi);

  /* DiscretePulseGenerator: '<Root>/Pulse Generator2' */
  rtb_PulseGenerator2 = ((rtDW.clockTickCounter_j < 50) &&
    (rtDW.clockTickCounter_j >= 0));
  if (rtDW.clockTickCounter_j >= 999) {
    rtDW.clockTickCounter_j = 0;
  } else {
    rtDW.clockTickCounter_j++;
  }

  /* End of DiscretePulseGenerator: '<Root>/Pulse Generator2' */

  /* DiscretePulseGenerator: '<Root>/Pulse Generator4' */
  rtb_LogicalOperator1_b = ((rtDW.clockTickCounter_m < 50) &&
    (rtDW.clockTickCounter_m >= 0));
  if (rtDW.clockTickCounter_m >= 999) {
    rtDW.clockTickCounter_m = 0;
  } else {
    rtDW.clockTickCounter_m++;
  }

  /* End of DiscretePulseGenerator: '<Root>/Pulse Generator4' */

  /* Logic: '<Root>/Logical Operator9' */
  rtDW.LogicalOperator9 = !(rtb_PulseGenerator2 || rtb_LogicalOperator1_b);

  /* S-Function (gpio_out): '<S21>/S-Function Builder' */
  gpio_out_Outputs_wrapper(&rtConstP.pooled12, &rtConstB.Subtract_gy,
    &rtDW.LogicalOperator9, &rtDW.SFunctionBuilder_DSTATE_gi);

  /* DiscretePulseGenerator: '<Root>/Pulse Generator3' */
  rtDW.PulseGenerator3 = ((rtDW.clockTickCounter_l < 1) &&
    (rtDW.clockTickCounter_l >= 0));
  if (rtDW.clockTickCounter_l >= 99) {
    rtDW.clockTickCounter_l = 0;
  } else {
    rtDW.clockTickCounter_l++;
  }

  /* End of DiscretePulseGenerator: '<Root>/Pulse Generator3' */

  /* S-Function (oled_draw): '<S40>/S-Function Builder' */
  oled_draw_Outputs_wrapper(&rtDW.PulseGenerator3,
    &rtDW.SFunctionBuilder_DSTATE_m5);

  /* S-Function "gpio_in_wrapper" Block: <S22>/S-Function Builder */
  gpio_in_Update_wrapper(&rtConstP.pooled11, &rtConstB.Subtract,
    &rtDW.SFunctionBuilder, &rtDW.SFunctionBuilder_DSTATE);

  /* S-Function "can_tx_wrapper" Block: <S4>/can_tx */
  can_tx_Update_wrapper(&rtDW.PulseGenerator, &rtConstP.Constant_Value_ku,
                        &rtConstP.Constant1_Value_p, &rtDW.MatrixConcatenate2[0],
                        &rtConstP.pooled16, &rtDW.can_tx_DSTATE);

  /* S-Function "can_rx_wrapper" Block: <S3>/S-Function Builder */
  can_rx_Update_wrapper(&rtConstP.Constant_Value_n, &rtConstP.pooled16,
                        &rtDW.SFunctionBuilder_o1, &rtDW.SFunctionBuilder_o2[0],
                        &rtDW.SFunctionBuilder_DSTATE_j);

  /* Update for DiscreteIntegrator: '<S6>/Discrete-Time Integrator' */
  rtDW.DiscreteTimeIntegrator_DSTATE += 0.001;
  rtDW.DiscreteTimeIntegrator_PrevRese = (int8_T)rtDW.SFunctionBuilder_o1;

  /* S-Function "gpio_out_wrapper" Block: <S9>/S-Function Builder */
  gpio_out_Update_wrapper(&rtConstP.pooled12, &rtConstB.Subtract_e,
    &rtDW.LogicalOperator3, &rtDW.SFunctionBuilder_DSTATE_j2);

  /* S-Function "gpio_out_wrapper" Block: <S12>/S-Function Builder */
  gpio_out_Update_wrapper(&rtConstP.pooled12, &rtConstB.Subtract_ep,
    &rtDW.LogicalOperator3_o, &rtDW.SFunctionBuilder_DSTATE_jo);

  /* S-Function "gpio_out_wrapper" Block: <S23>/S-Function Builder */
  gpio_out_Update_wrapper(&rtConstP.pooled11, &rtConstB.Subtract_b, &rtDW.In_j,
    &rtDW.SFunctionBuilder_DSTATE_d);

  /* Update for DiscreteIntegrator: '<Root>/Integrator Limited' incorporates:
   *  Gain: '<Root>/Gain1'
   *  Logic: '<S51>/Logical Operator1'
   *  Product: '<Root>/Product'
   */
  rtDW.IntegratorLimited_DSTATE += (rtDW.RelationalOperator1_n ||
    rtDW.RelationalOperator1_e ? -rtDW.QDIR : 0.0) * 0.001;
  if (rtDW.IntegratorLimited_DSTATE >= 0.1) {
    rtDW.IntegratorLimited_DSTATE = 0.1;
  } else {
    if (rtDW.IntegratorLimited_DSTATE <= 0.0) {
      rtDW.IntegratorLimited_DSTATE = 0.0;
    }
  }

  /* End of Update for DiscreteIntegrator: '<Root>/Integrator Limited' */

  /* S-Function "oled_idle_wrapper" Block: <Root>/S-Function Builder */
  oled_idle_Update_wrapper(&rtDW.idle, &rtDW.SFunctionBuilder_DSTATE_o);

  /* S-Function "oled_rtd_wrapper" Block: <Root>/S-Function Builder1 */
  oled_rtd_Update_wrapper(&rtDW.rtd, &rtDW.SFunctionBuilder1_DSTATE);

  /* S-Function "oled_driving_wrapper" Block: <Root>/S-Function Builder2 */
  oled_driving_Update_wrapper((real_T*)&model_RGND, (uint32_T*)&model_U32GND,
    (uint32_T*)&model_U32GND, (uint32_T*)&model_U32GND, (real_T*)&model_RGND,
    (uint32_T*)&model_U32GND, &rtDW.driving, &rtDW.SFunctionBuilder2_DSTATE);

  /* S-Function "can_rx_wrapper" Block: <S1>/S-Function Builder */
  can_rx_Update_wrapper(&rtConstP.Constant_Value_ks, &rtConstP.pooled16,
                        &rtDW.SFunctionBuilder_o1_a,
                        &rtDW.SFunctionBuilder_o2_f[0],
                        &rtDW.SFunctionBuilder_DSTATE_jo1);

  /* S-Function "oled_lv_wrapper" Block: <Root>/S-Function Builder3 */
  oled_lv_Update_wrapper(&rtDW.Gain2, &rtDW.Gain3, &rtDW.lv,
    &rtDW.SFunctionBuilder3_DSTATE);

  /* S-Function "oled_hv_wrapper" Block: <Root>/S-Function Builder4 */
  oled_hv_Update_wrapper(&rtConstP.Constant18_Value, &rtConstP.Constant19_Value,
    &rtConstP.Constant20_Value, &rtConstP.Constant21_Value,
    &rtConstP.Constant21_Value, &rtDW.hv, &rtDW.SFunctionBuilder4_DSTATE);

  /* S-Function "can_rx_wrapper" Block: <S2>/S-Function Builder */
  can_rx_Update_wrapper(&rtConstP.Constant_Value_j, &rtConstP.pooled16,
                        &rtDW.SFunctionBuilder_o1_c,
                        &rtDW.SFunctionBuilder_o2_a[0],
                        &rtDW.SFunctionBuilder_DSTATE_h);

  /* S-Function "oled_sdc_wrapper" Block: <Root>/S-Function Builder5 */
  oled_sdc_Update_wrapper(&rtDW.BittoIntegerConverter, &rtDW.sdc,
    &rtDW.SFunctionBuilder5_DSTATE);

  /* S-Function "oled_cooling_wrapper" Block: <Root>/S-Function Builder6 */
  oled_cooling_Update_wrapper((uint32_T*)&model_U32GND, (real_T*)&model_RGND,
    &rtDW.cooling, &rtDW.SFunctionBuilder6_DSTATE);

  /* S-Function "gpio_in_wrapper" Block: <S7>/S-Function Builder */
  gpio_in_Update_wrapper(&rtConstP.pooled11, &rtConstB.Subtract_c,
    &rtDW.SFunctionBuilder_e, &rtDW.SFunctionBuilder_DSTATE_ob);

  /* S-Function "gpio_in_wrapper" Block: <S8>/S-Function Builder */
  gpio_in_Update_wrapper(&rtConstP.pooled11, &rtConstB.Subtract_h,
    &rtDW.SFunctionBuilder_g, &rtDW.SFunctionBuilder_DSTATE_c);

  /* Update for UnitDelay: '<S48>/Unit Delay' */
  rtDW.UnitDelay_DSTATE = rtDW.SFunctionBuilder_e;

  /* Update for UnitDelay: '<S54>/Unit Delay' */
  rtDW.UnitDelay_DSTATE_h = rtDW.SFunctionBuilder_e;

  /* Update for UnitDelay: '<S57>/Unit Delay' */
  rtDW.UnitDelay_DSTATE_i = rtDW.SFunctionBuilder_g;

  /* Update for UnitDelay: '<S60>/Unit Delay' */
  rtDW.UnitDelay_DSTATE_o = rtDW.SFunctionBuilder_g;

  /* Update for UnitDelay: '<S51>/Unit Delay' incorporates:
   *  DataTypeConversion: '<S51>/Data Type Conversion2'
   */
  rtDW.UnitDelay_DSTATE_ho = (rtDW.QCLK != 0.0);

  /* S-Function "gpio_out_wrapper" Block: <S19>/S-Function Builder */
  gpio_out_Update_wrapper(&rtConstP.pooled12, &rtConstB.Subtract_k,
    &rtConstP.pooled18, &rtDW.SFunctionBuilder_DSTATE_m);

  /* S-Function "gpio_out_wrapper" Block: <S11>/S-Function Builder */
  gpio_out_Update_wrapper(&rtConstP.pooled11, &rtConstB.Subtract_g,
    &rtConstP.pooled17, &rtDW.SFunctionBuilder_DSTATE_i);

  /* S-Function "gpio_out_wrapper" Block: <S20>/S-Function Builder */
  gpio_out_Update_wrapper(&rtConstP.pooled12, &rtConstB.Subtract_i,
    &rtConstP.pooled18, &rtDW.SFunctionBuilder_DSTATE_oe);

  /* S-Function "gpio_out_wrapper" Block: <S13>/S-Function Builder */
  gpio_out_Update_wrapper(&rtConstP.pooled12, &rtConstB.Subtract_f,
    &rtConstP.pooled17, &rtDW.SFunctionBuilder_DSTATE_dx);

  /* S-Function "gpio_out_wrapper" Block: <S17>/S-Function Builder */
  gpio_out_Update_wrapper(&rtConstP.pooled11, &rtConstB.Subtract_go,
    &rtConstP.pooled17, &rtDW.SFunctionBuilder_DSTATE_e);

  /* S-Function "gpio_out_wrapper" Block: <S10>/S-Function Builder */
  gpio_out_Update_wrapper(&rtConstP.Constant_Value_k, &rtConstB.Subtract_n,
    &rtConstP.pooled17, &rtDW.SFunctionBuilder_DSTATE_a);

  /* S-Function "gpio_out_wrapper" Block: <S15>/S-Function Builder */
  gpio_out_Update_wrapper(&rtConstP.pooled11, &rtConstB.Subtract_fg,
    &rtConstP.pooled17, &rtDW.SFunctionBuilder_DSTATE_g);

  /* S-Function "gpio_out_wrapper" Block: <S18>/S-Function Builder */
  gpio_out_Update_wrapper(&rtConstP.pooled12, &rtConstB.Subtract_l,
    &rtConstP.pooled17, &rtDW.SFunctionBuilder_DSTATE_p);

  /* S-Function "gpio_out_wrapper" Block: <S14>/S-Function Builder */
  gpio_out_Update_wrapper(&rtConstP.pooled12, &rtConstB.Subtract_hi,
    &rtConstP.pooled17, &rtDW.SFunctionBuilder_DSTATE_n);

  /* S-Function "gpio_out_wrapper" Block: <S16>/S-Function Builder */
  gpio_out_Update_wrapper(&rtConstP.pooled12, &rtConstB.Subtract_gi,
    &rtConstP.pooled17, &rtDW.SFunctionBuilder_DSTATE_hi);

  /* S-Function "gpio_out_wrapper" Block: <S21>/S-Function Builder */
  gpio_out_Update_wrapper(&rtConstP.pooled12, &rtConstB.Subtract_gy,
    &rtDW.LogicalOperator9, &rtDW.SFunctionBuilder_DSTATE_gi);

  /* S-Function "oled_draw_wrapper" Block: <S40>/S-Function Builder */
  oled_draw_Update_wrapper(&rtDW.PulseGenerator3,
    &rtDW.SFunctionBuilder_DSTATE_m5);
  if (rtM->Timing.TaskCounters.TID[1] == 0) {
    /* Update absolute timer for sample time: [1.0s, 0.0s] */
    /* The "clockTick1" counts the number of times the code of this task has
     * been executed. The resolution of this integer timer is 1.0, which is the step size
     * of the task. Size of "clockTick1" ensures timer will not overflow during the
     * application lifespan selected.
     */
    rtM->Timing.clockTick1++;
  }

  rate_scheduler();
}

/* Model initialize function */
void model_initialize(void)
{
  /*-----------S-Function Block: <Root>/CAN Unpack2 -----------------*/

  /*-----------S-Function Block: <Root>/CAN Unpack1 -----------------*/

  /*-----------S-Function Block: <Root>/CAN Unpack3 -----------------*/

  /*-----------S-Function Block: <Root>/CAN Unpack4 -----------------*/
  rtPrevZCX.SampleandHold1_Trig_ZCE = POS_ZCSIG;
  rtPrevZCX.SampleandHold10_Trig_ZCE = POS_ZCSIG;
  rtPrevZCX.SampleandHold11_Trig_ZCE = POS_ZCSIG;
  rtPrevZCX.SampleandHold12_Trig_ZCE = POS_ZCSIG;
  rtPrevZCX.SampleandHold13_Trig_ZCE = POS_ZCSIG;
  rtPrevZCX.SampleandHold2_Trig_ZCE = POS_ZCSIG;
  rtPrevZCX.SampleandHold3_Trig_ZCE = POS_ZCSIG;
  rtPrevZCX.SampleandHold4_Trig_ZCE = POS_ZCSIG;
  rtPrevZCX.SampleandHold5_Trig_ZCE = POS_ZCSIG;
  rtPrevZCX.SampleandHold6_Trig_ZCE = POS_ZCSIG;
  rtPrevZCX.SampleandHold7_Trig_ZCE = POS_ZCSIG;
  rtPrevZCX.SampleandHold8_Trig_ZCE = POS_ZCSIG;
  rtPrevZCX.SampleandHold9_Trig_ZCE = POS_ZCSIG;

  /* S-Function Block: <S22>/S-Function Builder */
  {
    real_T initVector[1] = { 0 };

    {
      int_T i1;
      for (i1=0; i1 < 1; i1++) {
        rtDW.SFunctionBuilder_DSTATE = initVector[0];
      }
    }
  }

  /* S-Function Block: <S4>/can_tx */
  {
    real_T initVector[1] = { 0 };

    {
      int_T i1;
      for (i1=0; i1 < 1; i1++) {
        rtDW.can_tx_DSTATE = initVector[0];
      }
    }
  }

  /* S-Function Block: <S3>/S-Function Builder */
  {
    real_T initVector[1] = { 0 };

    {
      int_T i1;
      for (i1=0; i1 < 1; i1++) {
        rtDW.SFunctionBuilder_DSTATE_j = initVector[0];
      }
    }
  }

  /* InitializeConditions for DiscreteIntegrator: '<S6>/Discrete-Time Integrator' */
  rtDW.DiscreteTimeIntegrator_PrevRese = 2;

  /* S-Function Block: <S9>/S-Function Builder */
  {
    real_T initVector[1] = { 0 };

    {
      int_T i1;
      for (i1=0; i1 < 1; i1++) {
        rtDW.SFunctionBuilder_DSTATE_j2 = initVector[0];
      }
    }
  }

  /* S-Function Block: <S12>/S-Function Builder */
  {
    real_T initVector[1] = { 0 };

    {
      int_T i1;
      for (i1=0; i1 < 1; i1++) {
        rtDW.SFunctionBuilder_DSTATE_jo = initVector[0];
      }
    }
  }

  /* S-Function Block: <S23>/S-Function Builder */
  {
    real_T initVector[1] = { 0 };

    {
      int_T i1;
      for (i1=0; i1 < 1; i1++) {
        rtDW.SFunctionBuilder_DSTATE_d = initVector[0];
      }
    }
  }

  /* S-Function Block: <Root>/S-Function Builder */
  {
    real_T initVector[1] = { 0 };

    {
      int_T i1;
      for (i1=0; i1 < 1; i1++) {
        rtDW.SFunctionBuilder_DSTATE_o = initVector[0];
      }
    }
  }

  /* S-Function Block: <Root>/S-Function Builder1 */
  {
    real_T initVector[1] = { 0 };

    {
      int_T i1;
      for (i1=0; i1 < 1; i1++) {
        rtDW.SFunctionBuilder1_DSTATE = initVector[0];
      }
    }
  }

  /* S-Function Block: <Root>/S-Function Builder2 */
  {
    real_T initVector[1] = { 0 };

    {
      int_T i1;
      for (i1=0; i1 < 1; i1++) {
        rtDW.SFunctionBuilder2_DSTATE = initVector[0];
      }
    }
  }

  /* S-Function Block: <S1>/S-Function Builder */
  {
    real_T initVector[1] = { 0 };

    {
      int_T i1;
      for (i1=0; i1 < 1; i1++) {
        rtDW.SFunctionBuilder_DSTATE_jo1 = initVector[0];
      }
    }
  }

  /* S-Function Block: <Root>/S-Function Builder3 */
  {
    real_T initVector[1] = { 0 };

    {
      int_T i1;
      for (i1=0; i1 < 1; i1++) {
        rtDW.SFunctionBuilder3_DSTATE = initVector[0];
      }
    }
  }

  /* S-Function Block: <Root>/S-Function Builder4 */
  {
    real_T initVector[1] = { 0 };

    {
      int_T i1;
      for (i1=0; i1 < 1; i1++) {
        rtDW.SFunctionBuilder4_DSTATE = initVector[0];
      }
    }
  }

  /* S-Function Block: <S2>/S-Function Builder */
  {
    real_T initVector[1] = { 0 };

    {
      int_T i1;
      for (i1=0; i1 < 1; i1++) {
        rtDW.SFunctionBuilder_DSTATE_h = initVector[0];
      }
    }
  }

  /* S-Function Block: <Root>/S-Function Builder5 */
  {
    real_T initVector[1] = { 0 };

    {
      int_T i1;
      for (i1=0; i1 < 1; i1++) {
        rtDW.SFunctionBuilder5_DSTATE = initVector[0];
      }
    }
  }

  /* S-Function Block: <Root>/S-Function Builder6 */
  {
    real_T initVector[1] = { 0 };

    {
      int_T i1;
      for (i1=0; i1 < 1; i1++) {
        rtDW.SFunctionBuilder6_DSTATE = initVector[0];
      }
    }
  }

  /* S-Function Block: <S7>/S-Function Builder */
  {
    real_T initVector[1] = { 0 };

    {
      int_T i1;
      for (i1=0; i1 < 1; i1++) {
        rtDW.SFunctionBuilder_DSTATE_ob = initVector[0];
      }
    }
  }

  /* S-Function Block: <S8>/S-Function Builder */
  {
    real_T initVector[1] = { 0 };

    {
      int_T i1;
      for (i1=0; i1 < 1; i1++) {
        rtDW.SFunctionBuilder_DSTATE_c = initVector[0];
      }
    }
  }

  /* InitializeConditions for UnitDelay: '<S54>/Unit Delay' */
  rtDW.UnitDelay_DSTATE_h = true;

  /* InitializeConditions for UnitDelay: '<S60>/Unit Delay' */
  rtDW.UnitDelay_DSTATE_o = true;

  /* S-Function Block: <S19>/S-Function Builder */
  {
    real_T initVector[1] = { 0 };

    {
      int_T i1;
      for (i1=0; i1 < 1; i1++) {
        rtDW.SFunctionBuilder_DSTATE_m = initVector[0];
      }
    }
  }

  /* S-Function Block: <S11>/S-Function Builder */
  {
    real_T initVector[1] = { 0 };

    {
      int_T i1;
      for (i1=0; i1 < 1; i1++) {
        rtDW.SFunctionBuilder_DSTATE_i = initVector[0];
      }
    }
  }

  /* S-Function Block: <S20>/S-Function Builder */
  {
    real_T initVector[1] = { 0 };

    {
      int_T i1;
      for (i1=0; i1 < 1; i1++) {
        rtDW.SFunctionBuilder_DSTATE_oe = initVector[0];
      }
    }
  }

  /* S-Function Block: <S13>/S-Function Builder */
  {
    real_T initVector[1] = { 0 };

    {
      int_T i1;
      for (i1=0; i1 < 1; i1++) {
        rtDW.SFunctionBuilder_DSTATE_dx = initVector[0];
      }
    }
  }

  /* S-Function Block: <S17>/S-Function Builder */
  {
    real_T initVector[1] = { 0 };

    {
      int_T i1;
      for (i1=0; i1 < 1; i1++) {
        rtDW.SFunctionBuilder_DSTATE_e = initVector[0];
      }
    }
  }

  /* S-Function Block: <S10>/S-Function Builder */
  {
    real_T initVector[1] = { 0 };

    {
      int_T i1;
      for (i1=0; i1 < 1; i1++) {
        rtDW.SFunctionBuilder_DSTATE_a = initVector[0];
      }
    }
  }

  /* S-Function Block: <S15>/S-Function Builder */
  {
    real_T initVector[1] = { 0 };

    {
      int_T i1;
      for (i1=0; i1 < 1; i1++) {
        rtDW.SFunctionBuilder_DSTATE_g = initVector[0];
      }
    }
  }

  /* S-Function Block: <S18>/S-Function Builder */
  {
    real_T initVector[1] = { 0 };

    {
      int_T i1;
      for (i1=0; i1 < 1; i1++) {
        rtDW.SFunctionBuilder_DSTATE_p = initVector[0];
      }
    }
  }

  /* S-Function Block: <S14>/S-Function Builder */
  {
    real_T initVector[1] = { 0 };

    {
      int_T i1;
      for (i1=0; i1 < 1; i1++) {
        rtDW.SFunctionBuilder_DSTATE_n = initVector[0];
      }
    }
  }

  /* S-Function Block: <S16>/S-Function Builder */
  {
    real_T initVector[1] = { 0 };

    {
      int_T i1;
      for (i1=0; i1 < 1; i1++) {
        rtDW.SFunctionBuilder_DSTATE_hi = initVector[0];
      }
    }
  }

  /* InitializeConditions for DiscretePulseGenerator: '<Root>/Pulse Generator4' */
  rtDW.clockTickCounter_m = -100;

  /* S-Function Block: <S21>/S-Function Builder */
  {
    real_T initVector[1] = { 0 };

    {
      int_T i1;
      for (i1=0; i1 < 1; i1++) {
        rtDW.SFunctionBuilder_DSTATE_gi = initVector[0];
      }
    }
  }

  /* S-Function Block: <S40>/S-Function Builder */
  {
    real_T initVector[1] = { 0 };

    {
      int_T i1;
      for (i1=0; i1 < 1; i1++) {
        rtDW.SFunctionBuilder_DSTATE_m5 = initVector[0];
      }
    }
  }
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
