/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * File: model.h
 *
 * Code generated for Simulink model 'model'.
 *
 * Model version                  : 1.325
 * Simulink Coder version         : 8.11 (R2016b) 25-Aug-2016
 * C/C++ source code generated on : Thu Jun 14 18:42:28 2018
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: ARM Compatible->ARM Cortex
 * Code generation objectives:
 *    1. Execution efficiency
 *    2. RAM efficiency
 * Validation result: Not run
 */

#ifndef RTW_HEADER_model_h_
#define RTW_HEADER_model_h_
#include <string.h>
#ifndef model_COMMON_INCLUDES_
# define model_COMMON_INCLUDES_
#include <string.h>
#include "rtwtypes.h"
#include "zero_crossing_types.h"
#include "rtw_continuous.h"
#include "rtw_solver.h"
#include "can_message.h"
#endif                                 /* model_COMMON_INCLUDES_ */

/* Macros for accessing real-time model data structure */
#ifndef rtmGetErrorStatus
# define rtmGetErrorStatus(rtm)        ((rtm)->errorStatus)
#endif

#ifndef rtmSetErrorStatus
# define rtmSetErrorStatus(rtm, val)   ((rtm)->errorStatus = (val))
#endif

/* Forward declaration for rtModel */
typedef struct tag_RTM RT_MODEL;

/* user code (top of export header file) */
#include "can_message.h"

/* Block signals and states (auto storage) for system '<Root>' */
typedef struct {
  CAN_DATATYPE CANPack2;               /* '<Root>/CAN Pack2' */
  CAN_DATATYPE CANPack1;               /* '<Root>/CAN Pack1' */
  CAN_DATATYPE CANPack3;               /* '<Root>/CAN Pack3' */
  CAN_DATATYPE CANPack4;               /* '<Root>/CAN Pack4' */
  real_T VectorConcatenate[9];         /* '<Root>/Vector Concatenate' */
  real_T CANUnpack1_o1;                /* '<Root>/CAN Unpack1' */
  real_T CANUnpack1_o2;                /* '<Root>/CAN Unpack1' */
  real_T CANUnpack1_o3;                /* '<Root>/CAN Unpack1' */
  real_T CANUnpack1_o4;                /* '<Root>/CAN Unpack1' */
  real_T CANUnpack1_o5;                /* '<Root>/CAN Unpack1' */
  real_T CANUnpack1_o7;                /* '<Root>/CAN Unpack1' */
  real_T Step2;                        /* '<S38>/Step2' */
  real_T Step2_p;                      /* '<S39>/Step2' */
  real_T CANUnpack3_o1;                /* '<Root>/CAN Unpack3' */
  real_T CANUnpack3_o2;                /* '<Root>/CAN Unpack3' */
  real_T CANUnpack3_o3;                /* '<Root>/CAN Unpack3' */
  real_T CANUnpack3_o4;                /* '<Root>/CAN Unpack3' */
  real_T CANUnpack3_o5;                /* '<Root>/CAN Unpack3' */
  real_T CANUnpack3_o6;                /* '<Root>/CAN Unpack3' */
  real_T CANUnpack3_o7;                /* '<Root>/CAN Unpack3' */
  real_T CANUnpack3_o8;                /* '<Root>/CAN Unpack3' */
  real_T Gain2;                        /* '<Root>/Gain2' */
  real_T CANUnpack4_o1;                /* '<Root>/CAN Unpack4' */
  real_T CANUnpack4_o2;                /* '<Root>/CAN Unpack4' */
  real_T CANUnpack4_o3;                /* '<Root>/CAN Unpack4' */
  real_T CANUnpack4_o4;                /* '<Root>/CAN Unpack4' */
  real_T CANUnpack4_o5;                /* '<Root>/CAN Unpack4' */
  real_T CANUnpack4_o6;                /* '<Root>/CAN Unpack4' */
  real_T CANUnpack4_o7;                /* '<Root>/CAN Unpack4' */
  real_T CANUnpack4_o8;                /* '<Root>/CAN Unpack4' */
  real_T CANUnpack4_o9;                /* '<Root>/CAN Unpack4' */
  real_T CANUnpack4_o10;               /* '<Root>/CAN Unpack4' */
  real_T In;                           /* '<S32>/In' */
  real_T In_f;                         /* '<S30>/In' */
  real_T In_i;                         /* '<S25>/In' */
  real_T QCLK;                         /* '<S24>/Encoder States' */
  real_T QDIR;                         /* '<S24>/Encoder States' */
  real_T SFunctionBuilder_DSTATE;      /* '<S22>/S-Function Builder' */
  real_T can_tx_DSTATE;                /* '<S4>/can_tx' */
  real_T SFunctionBuilder_DSTATE_j;    /* '<S3>/S-Function Builder' */
  real_T DiscreteTimeIntegrator_DSTATE;/* '<S6>/Discrete-Time Integrator' */
  real_T SFunctionBuilder_DSTATE_j2;   /* '<S9>/S-Function Builder' */
  real_T SFunctionBuilder_DSTATE_jo;   /* '<S12>/S-Function Builder' */
  real_T SFunctionBuilder_DSTATE_d;    /* '<S23>/S-Function Builder' */
  real_T IntegratorLimited_DSTATE;     /* '<Root>/Integrator Limited' */
  real_T SFunctionBuilder_DSTATE_o;    /* '<Root>/S-Function Builder' */
  real_T SFunctionBuilder1_DSTATE;     /* '<Root>/S-Function Builder1' */
  real_T SFunctionBuilder2_DSTATE;     /* '<Root>/S-Function Builder2' */
  real_T SFunctionBuilder_DSTATE_jo1;  /* '<S1>/S-Function Builder' */
  real_T SFunctionBuilder3_DSTATE;     /* '<Root>/S-Function Builder3' */
  real_T SFunctionBuilder4_DSTATE;     /* '<Root>/S-Function Builder4' */
  real_T SFunctionBuilder_DSTATE_h;    /* '<S2>/S-Function Builder' */
  real_T SFunctionBuilder5_DSTATE;     /* '<Root>/S-Function Builder5' */
  real_T SFunctionBuilder6_DSTATE;     /* '<Root>/S-Function Builder6' */
  real_T SFunctionBuilder_DSTATE_ob;   /* '<S7>/S-Function Builder' */
  real_T SFunctionBuilder_DSTATE_c;    /* '<S8>/S-Function Builder' */
  real_T SFunctionBuilder_DSTATE_m;    /* '<S19>/S-Function Builder' */
  real_T SFunctionBuilder_DSTATE_i;    /* '<S11>/S-Function Builder' */
  real_T SFunctionBuilder_DSTATE_oe;   /* '<S20>/S-Function Builder' */
  real_T SFunctionBuilder_DSTATE_dx;   /* '<S13>/S-Function Builder' */
  real_T SFunctionBuilder_DSTATE_e;    /* '<S17>/S-Function Builder' */
  real_T SFunctionBuilder_DSTATE_a;    /* '<S10>/S-Function Builder' */
  real_T SFunctionBuilder_DSTATE_g;    /* '<S15>/S-Function Builder' */
  real_T SFunctionBuilder_DSTATE_p;    /* '<S18>/S-Function Builder' */
  real_T SFunctionBuilder_DSTATE_n;    /* '<S14>/S-Function Builder' */
  real_T SFunctionBuilder_DSTATE_hi;   /* '<S16>/S-Function Builder' */
  real_T SFunctionBuilder_DSTATE_gi;   /* '<S21>/S-Function Builder' */
  real_T SFunctionBuilder_DSTATE_m5;   /* '<S40>/S-Function Builder' */
  int32_T clockTickCounter;            /* '<Root>/Pulse Generator' */
  int32_T clockTickCounter_j;          /* '<Root>/Pulse Generator2' */
  int32_T clockTickCounter_m;          /* '<Root>/Pulse Generator4' */
  int32_T clockTickCounter_l;          /* '<Root>/Pulse Generator3' */
  int_T CANPack2_ModeSignalID;         /* '<Root>/CAN Pack2' */
  int_T CANUnpack2_ModeSignalID;       /* '<Root>/CAN Unpack2' */
  int_T CANUnpack2_StatusPortID;       /* '<Root>/CAN Unpack2' */
  int_T CANUnpack1_ModeSignalID;       /* '<Root>/CAN Unpack1' */
  int_T CANUnpack1_StatusPortID;       /* '<Root>/CAN Unpack1' */
  int_T CANUnpack3_ModeSignalID;       /* '<Root>/CAN Unpack3' */
  int_T CANUnpack3_StatusPortID;       /* '<Root>/CAN Unpack3' */
  int_T CANUnpack4_ModeSignalID;       /* '<Root>/CAN Unpack4' */
  int_T CANUnpack4_StatusPortID;       /* '<Root>/CAN Unpack4' */
  uint32_T Gain3;                      /* '<Root>/Gain3' */
  uint32_T BittoIntegerConverter;      /* '<Root>/Bit to Integer Converter' */
  uint16_T temporalCounter_i1;         /* '<Root>/Chart' */
  int8_T DiscreteTimeIntegrator_PrevRese;/* '<S6>/Discrete-Time Integrator' */
  uint8_T MatrixConcatenate2[8];       /* '<Root>/Matrix Concatenate2' */
  uint8_T SFunctionBuilder_o2[8];      /* '<S3>/S-Function Builder' */
  uint8_T SFunctionBuilder_o2_f[8];    /* '<S1>/S-Function Builder' */
  uint8_T SFunctionBuilder_o2_a[8];    /* '<S2>/S-Function Builder' */
  uint8_T is_active_c3_model;          /* '<S24>/Encoder States' */
  uint8_T is_c3_model;                 /* '<S24>/Encoder States' */
  uint8_T is_active_c1_model;          /* '<Root>/Chart' */
  uint8_T is_c1_model;                 /* '<Root>/Chart' */
  boolean_T PulseGenerator;            /* '<Root>/Pulse Generator' */
  boolean_T SFunctionBuilder;          /* '<S22>/S-Function Builder' */
  boolean_T SFunctionBuilder_o1;       /* '<S3>/S-Function Builder' */
  boolean_T CANUnpack1_o6;             /* '<Root>/CAN Unpack1' */
  boolean_T LogicalOperator3;          /* '<S38>/Logical Operator3' */
  boolean_T LogicalOperator3_o;        /* '<S39>/Logical Operator3' */
  boolean_T SFunctionBuilder_o1_a;     /* '<S1>/S-Function Builder' */
  boolean_T SFunctionBuilder_o1_c;     /* '<S2>/S-Function Builder' */
  boolean_T SFunctionBuilder_e;        /* '<S7>/S-Function Builder' */
  boolean_T SFunctionBuilder_g;        /* '<S8>/S-Function Builder' */
  boolean_T LogicalOperator9;          /* '<Root>/Logical Operator9' */
  boolean_T PulseGenerator3;           /* '<Root>/Pulse Generator3' */
  boolean_T In_j;                      /* '<S31>/In' */
  boolean_T RelationalOperator1;       /* '<S62>/Relational Operator1' */
  boolean_T RelationalOperator1_g;     /* '<S61>/Relational Operator1' */
  boolean_T RelationalOperator1_m;     /* '<S59>/Relational Operator1' */
  boolean_T RelationalOperator1_h;     /* '<S58>/Relational Operator1' */
  boolean_T RelationalOperator1_a;     /* '<S56>/Relational Operator1' */
  boolean_T RelationalOperator1_l;     /* '<S55>/Relational Operator1' */
  boolean_T RelationalOperator1_n;     /* '<S53>/Relational Operator1' */
  boolean_T RelationalOperator1_e;     /* '<S52>/Relational Operator1' */
  boolean_T RelationalOperator1_n3;    /* '<S50>/Relational Operator1' */
  boolean_T RelationalOperator1_i;     /* '<S49>/Relational Operator1' */
  boolean_T cooling;                   /* '<Root>/Chart' */
  boolean_T driving;                   /* '<Root>/Chart' */
  boolean_T hv;                        /* '<Root>/Chart' */
  boolean_T idle;                      /* '<Root>/Chart' */
  boolean_T lv;                        /* '<Root>/Chart' */
  boolean_T rtd;                       /* '<Root>/Chart' */
  boolean_T sdc;                       /* '<Root>/Chart' */
  boolean_T UnitDelay_DSTATE;          /* '<S48>/Unit Delay' */
  boolean_T UnitDelay_DSTATE_h;        /* '<S54>/Unit Delay' */
  boolean_T UnitDelay_DSTATE_i;        /* '<S57>/Unit Delay' */
  boolean_T UnitDelay_DSTATE_o;        /* '<S60>/Unit Delay' */
  boolean_T UnitDelay_DSTATE_ho;       /* '<S51>/Unit Delay' */
} DW;

/* Zero-crossing (trigger) state */
typedef struct {
  ZCSigState SampleandHold9_Trig_ZCE;  /* '<Root>/Sample and Hold9' */
  ZCSigState SampleandHold8_Trig_ZCE;  /* '<Root>/Sample and Hold8' */
  ZCSigState SampleandHold7_Trig_ZCE;  /* '<Root>/Sample and Hold7' */
  ZCSigState SampleandHold6_Trig_ZCE;  /* '<Root>/Sample and Hold6' */
  ZCSigState SampleandHold5_Trig_ZCE;  /* '<Root>/Sample and Hold5' */
  ZCSigState SampleandHold4_Trig_ZCE;  /* '<Root>/Sample and Hold4' */
  ZCSigState SampleandHold3_Trig_ZCE;  /* '<Root>/Sample and Hold3' */
  ZCSigState SampleandHold2_Trig_ZCE;  /* '<Root>/Sample and Hold2' */
  ZCSigState SampleandHold13_Trig_ZCE; /* '<Root>/Sample and Hold13' */
  ZCSigState SampleandHold12_Trig_ZCE; /* '<Root>/Sample and Hold12' */
  ZCSigState SampleandHold11_Trig_ZCE; /* '<Root>/Sample and Hold11' */
  ZCSigState SampleandHold10_Trig_ZCE; /* '<Root>/Sample and Hold10' */
  ZCSigState SampleandHold1_Trig_ZCE;  /* '<Root>/Sample and Hold1' */
} PrevZCX;

/* Invariant block signals (auto storage) */
typedef struct {
  const int32_T Subtract;              /* '<S22>/Subtract' */
  const int32_T Subtract_e;            /* '<S9>/Subtract' */
  const int32_T Subtract_ep;           /* '<S12>/Subtract' */
  const int32_T Subtract_b;            /* '<S23>/Subtract' */
  const int32_T Subtract_c;            /* '<S7>/Subtract' */
  const int32_T Subtract_h;            /* '<S8>/Subtract' */
  const int32_T Subtract_k;            /* '<S19>/Subtract' */
  const int32_T Subtract_g;            /* '<S11>/Subtract' */
  const int32_T Subtract_i;            /* '<S20>/Subtract' */
  const int32_T Subtract_f;            /* '<S13>/Subtract' */
  const int32_T Subtract_go;           /* '<S17>/Subtract' */
  const int32_T Subtract_n;            /* '<S10>/Subtract' */
  const int32_T Subtract_fg;           /* '<S15>/Subtract' */
  const int32_T Subtract_l;            /* '<S18>/Subtract' */
  const int32_T Subtract_hi;           /* '<S14>/Subtract' */
  const int32_T Subtract_gi;           /* '<S16>/Subtract' */
  const int32_T Subtract_gy;           /* '<S21>/Subtract' */
} ConstB;

/* Constant parameters (auto storage) */
typedef struct {
  /* Expression: 4.12
   * Referenced by: '<Root>/Constant19'
   */
  real_T Constant19_Value;

  /* Expression: 4.09
   * Referenced by: '<Root>/Constant20'
   */
  real_T Constant20_Value;

  /* Expression: 35.0
   * Referenced by: '<Root>/Constant21'
   */
  real_T Constant21_Value;

  /* Pooled Parameter (Expression: )
   * Referenced by:
   *   '<S7>/Constant'
   *   '<S7>/Constant2'
   *   '<S8>/Constant'
   *   '<S8>/Constant2'
   *   '<S9>/Constant2'
   *   '<S10>/Constant2'
   *   '<S11>/Constant'
   *   '<S11>/Constant2'
   *   '<S12>/Constant2'
   *   '<S13>/Constant2'
   *   '<S14>/Constant2'
   *   '<S15>/Constant'
   *   '<S15>/Constant2'
   *   '<S16>/Constant2'
   *   '<S17>/Constant'
   *   '<S17>/Constant2'
   *   '<S18>/Constant2'
   *   '<S19>/Constant1'
   *   '<S19>/Constant2'
   *   '<S20>/Constant2'
   *   '<S21>/Constant2'
   *   '<S22>/Constant'
   *   '<S22>/Constant2'
   *   '<S23>/Constant'
   *   '<S23>/Constant2'
   */
  int32_T pooled11;

  /* Pooled Parameter (Expression: )
   * Referenced by:
   *   '<S9>/Constant'
   *   '<S10>/Constant1'
   *   '<S12>/Constant'
   *   '<S13>/Constant'
   *   '<S13>/Constant1'
   *   '<S14>/Constant'
   *   '<S16>/Constant'
   *   '<S18>/Constant'
   *   '<S19>/Constant'
   *   '<S20>/Constant'
   *   '<S21>/Constant'
   */
  int32_T pooled12;

  /* Computed Parameter: Constant_Value_k
   * Referenced by: '<S10>/Constant'
   */
  int32_T Constant_Value_k;

  /* Computed Parameter: Constant_Value_ku
   * Referenced by: '<S4>/Constant'
   */
  uint32_T Constant_Value_ku;

  /* Computed Parameter: Constant1_Value_p
   * Referenced by: '<S4>/Constant1'
   */
  uint32_T Constant1_Value_p;

  /* Pooled Parameter (Expression: )
   * Referenced by:
   *   '<S1>/Constant1'
   *   '<S2>/Constant1'
   *   '<S3>/Constant1'
   *   '<S4>/Constant2'
   */
  uint32_T pooled16;

  /* Computed Parameter: Constant_Value_n
   * Referenced by: '<S3>/Constant'
   */
  uint32_T Constant_Value_n;

  /* Computed Parameter: Constant_Value_ks
   * Referenced by: '<S1>/Constant'
   */
  uint32_T Constant_Value_ks;

  /* Computed Parameter: Constant18_Value
   * Referenced by: '<Root>/Constant18'
   */
  uint32_T Constant18_Value;

  /* Computed Parameter: Constant_Value_j
   * Referenced by: '<S2>/Constant'
   */
  uint32_T Constant_Value_j;

  /* Pooled Parameter (Mixed Expressions)
   * Referenced by:
   *   '<Root>/Constant1'
   *   '<Root>/Constant12'
   *   '<Root>/Constant13'
   *   '<Root>/Constant2'
   *   '<Root>/Constant3'
   *   '<Root>/Constant4'
   *   '<Root>/Constant6'
   *   '<Root>/Constant7'
   *   '<S31>/ '
   *   '<S48>/Unit Delay'
   *   '<S51>/Unit Delay'
   *   '<S57>/Unit Delay'
   *   '<S49>/OUT'
   *   '<S50>/OUT'
   *   '<S52>/OUT'
   *   '<S53>/OUT'
   *   '<S55>/OUT'
   *   '<S56>/OUT'
   *   '<S58>/OUT'
   *   '<S59>/OUT'
   *   '<S61>/OUT'
   *   '<S62>/OUT'
   */
  boolean_T pooled17;

  /* Pooled Parameter (Mixed Expressions)
   * Referenced by:
   *   '<Root>/Constant'
   *   '<Root>/Constant10'
   *   '<Root>/Pulse Generator'
   *   '<Root>/Pulse Generator2'
   *   '<Root>/Pulse Generator3'
   *   '<Root>/Pulse Generator4'
   *   '<S54>/Unit Delay'
   *   '<S60>/Unit Delay'
   */
  boolean_T pooled18;
} ConstP;

/* Real-time Model Data Structure */
struct tag_RTM {
  const char_T * volatile errorStatus;

  /*
   * Timing:
   * The following substructure contains information regarding
   * the timing information for the model.
   */
  struct {
    uint32_T clockTick1;
    struct {
      uint16_T TID[2];
    } TaskCounters;
  } Timing;
};

/* Block signals and states (auto storage) */
extern DW rtDW;

/* External data declarations for dependent source files */
extern const real_T model_RGND;        /* real_T ground */
extern const uint32_T model_U32GND;    /* uint32_T ground */
extern const ConstB rtConstB;          /* constant block i/o */

/* Constant parameters (auto storage) */
extern const ConstP rtConstP;

/* Model entry point functions */
extern void model_initialize(void);
extern void model_step(void);

/* Real-time Model object */
extern RT_MODEL *const rtM;

/*-
 * These blocks were eliminated from the model due to optimizations:
 *
 * Block '<S48>/Data Type Conversion2' : Eliminate redundant data type conversion
 * Block '<S48>/Multiport Switch' : Eliminated due to constant selection input
 * Block '<S51>/Multiport Switch' : Eliminated due to constant selection input
 * Block '<S54>/Data Type Conversion2' : Eliminate redundant data type conversion
 * Block '<S54>/Multiport Switch' : Eliminated due to constant selection input
 * Block '<S57>/Data Type Conversion2' : Eliminate redundant data type conversion
 * Block '<S57>/Multiport Switch' : Eliminated due to constant selection input
 * Block '<S60>/Data Type Conversion2' : Eliminate redundant data type conversion
 * Block '<S60>/Multiport Switch' : Eliminated due to constant selection input
 * Block '<S48>/Constant1' : Unused code path elimination
 * Block '<S48>/either edge' : Unused code path elimination
 * Block '<S48>/neg. edge' : Unused code path elimination
 * Block '<S51>/Constant1' : Unused code path elimination
 * Block '<S51>/either edge' : Unused code path elimination
 * Block '<S51>/neg. edge' : Unused code path elimination
 * Block '<S54>/Constant1' : Unused code path elimination
 * Block '<S54>/either edge' : Unused code path elimination
 * Block '<S54>/pos. edge' : Unused code path elimination
 * Block '<S57>/Constant1' : Unused code path elimination
 * Block '<S57>/either edge' : Unused code path elimination
 * Block '<S57>/neg. edge' : Unused code path elimination
 * Block '<S60>/Constant1' : Unused code path elimination
 * Block '<S60>/either edge' : Unused code path elimination
 * Block '<S60>/pos. edge' : Unused code path elimination
 */

/*-
 * The generated code includes comments that allow you to trace directly
 * back to the appropriate location in the model.  The basic format
 * is <system>/block_name, where system is the system number (uniquely
 * assigned by Simulink) and block_name is the name of the block.
 *
 * Use the MATLAB hilite_system command to trace the generated code back
 * to the model.  For example,
 *
 * hilite_system('<S3>')    - opens system 3
 * hilite_system('<S3>/Kp') - opens and selects block Kp which resides in S3
 *
 * Here is the system hierarchy for this model
 *
 * '<Root>' : 'model'
 * '<S1>'   : 'model/CAN Rx1'
 * '<S2>'   : 'model/CAN Rx2'
 * '<S3>'   : 'model/CAN Rx4'
 * '<S4>'   : 'model/CAN Tx4'
 * '<S5>'   : 'model/Chart'
 * '<S6>'   : 'model/Check for Timeout'
 * '<S7>'   : 'model/Encoder SIGNAL A'
 * '<S8>'   : 'model/Encoder SIGNAL B'
 * '<S9>'   : 'model/LED_AMS'
 * '<S10>'  : 'model/LED_ASR'
 * '<S11>'  : 'model/LED_DRS'
 * '<S12>'  : 'model/LED_IMD'
 * '<S13>'  : 'model/LED_LE'
 * '<S14>'  : 'model/LED_RECU'
 * '<S15>'  : 'model/LED_RESERVE'
 * '<S16>'  : 'model/LED_SDC'
 * '<S17>'  : 'model/LED_TEMP'
 * '<S18>'  : 'model/LED_TV'
 * '<S19>'  : 'model/LED_error'
 * '<S20>'  : 'model/LED_error1'
 * '<S21>'  : 'model/LED_heartbeat'
 * '<S22>'  : 'model/RTD Button'
 * '<S23>'  : 'model/RTD_sound'
 * '<S24>'  : 'model/Rotary Encoder Evaluation'
 * '<S25>'  : 'model/Sample and Hold1'
 * '<S26>'  : 'model/Sample and Hold10'
 * '<S27>'  : 'model/Sample and Hold11'
 * '<S28>'  : 'model/Sample and Hold12'
 * '<S29>'  : 'model/Sample and Hold13'
 * '<S30>'  : 'model/Sample and Hold2'
 * '<S31>'  : 'model/Sample and Hold3'
 * '<S32>'  : 'model/Sample and Hold4'
 * '<S33>'  : 'model/Sample and Hold5'
 * '<S34>'  : 'model/Sample and Hold6'
 * '<S35>'  : 'model/Sample and Hold7'
 * '<S36>'  : 'model/Sample and Hold8'
 * '<S37>'  : 'model/Sample and Hold9'
 * '<S38>'  : 'model/T 10.3.3 Compliance'
 * '<S39>'  : 'model/T 10.3.3 Compliance1'
 * '<S40>'  : 'model/redraw OLED Screen'
 * '<S41>'  : 'model/Check for Timeout/Compare To Constant'
 * '<S42>'  : 'model/Rotary Encoder Evaluation/Edge Detector'
 * '<S43>'  : 'model/Rotary Encoder Evaluation/Edge Detector1'
 * '<S44>'  : 'model/Rotary Encoder Evaluation/Edge Detector2'
 * '<S45>'  : 'model/Rotary Encoder Evaluation/Edge Detector3'
 * '<S46>'  : 'model/Rotary Encoder Evaluation/Edge Detector4'
 * '<S47>'  : 'model/Rotary Encoder Evaluation/Encoder States'
 * '<S48>'  : 'model/Rotary Encoder Evaluation/Edge Detector/Model'
 * '<S49>'  : 'model/Rotary Encoder Evaluation/Edge Detector/Model/NEGATIVE Edge'
 * '<S50>'  : 'model/Rotary Encoder Evaluation/Edge Detector/Model/POSITIVE Edge'
 * '<S51>'  : 'model/Rotary Encoder Evaluation/Edge Detector1/Model'
 * '<S52>'  : 'model/Rotary Encoder Evaluation/Edge Detector1/Model/NEGATIVE Edge'
 * '<S53>'  : 'model/Rotary Encoder Evaluation/Edge Detector1/Model/POSITIVE Edge'
 * '<S54>'  : 'model/Rotary Encoder Evaluation/Edge Detector2/Model'
 * '<S55>'  : 'model/Rotary Encoder Evaluation/Edge Detector2/Model/NEGATIVE Edge'
 * '<S56>'  : 'model/Rotary Encoder Evaluation/Edge Detector2/Model/POSITIVE Edge'
 * '<S57>'  : 'model/Rotary Encoder Evaluation/Edge Detector3/Model'
 * '<S58>'  : 'model/Rotary Encoder Evaluation/Edge Detector3/Model/NEGATIVE Edge'
 * '<S59>'  : 'model/Rotary Encoder Evaluation/Edge Detector3/Model/POSITIVE Edge'
 * '<S60>'  : 'model/Rotary Encoder Evaluation/Edge Detector4/Model'
 * '<S61>'  : 'model/Rotary Encoder Evaluation/Edge Detector4/Model/NEGATIVE Edge'
 * '<S62>'  : 'model/Rotary Encoder Evaluation/Edge Detector4/Model/POSITIVE Edge'
 */
#endif                                 /* RTW_HEADER_model_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
