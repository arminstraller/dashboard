/*
 * This file should conatain a implementation for every external function in model.c Usually Outputs_wrapper and Update_wrapper for every S Function
 * current Version of Simulink Blocks:
 * gpio_in			 -
 * gpio_out			 -
 * adc_average	 v1.1
 * dac1					 -
 * can_rx				 v1.1
 * can_tx				 v1.1
 */
#include "simulink_custom_s_function_wrapper.h"
#include "u8g2.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <font.h>

#define ADC_SAMPLECOUNT 50


#define LEFT 1
#define NEUTRAL 0
#define RIGHT 2

uint16_t samplearray[50];
int error = 0;

//OLED u8g2 object
u8g2_t u8g2;
//struct samplearray{
//  int samplecount;
//  int channelcount;
//  uint32_t* SampledValues;
//}samplearray;

//struct samplearray adc1_samplearray;
//struct samplearray adc2_samplearray;
//struct samplearray adc3_samplearray;

CAN_FilterConfTypeDef  sFilterConfig1;
CAN_FilterConfTypeDef  sFilterConfig2;


uint32_T can_rx_ids_channel1[MAX_CAN_RX_COUNT];
uint32_T can_rx_ids_channel2[MAX_CAN_RX_COUNT];

int can_tx_block_count=0;
int can1_rx_block_count=0;
int can2_rx_block_count=0;
int adc_average_block_count = 0;
int oled_draw_block_count  = 0;


static CanTxMsgTypeDef TxMessage1;
static CanRxMsgTypeDef RxMessage1;
static CanTxMsgTypeDef TxMessage2;
static CanRxMsgTypeDef RxMessage2;

can_rx_model_data* can_rx_model_data_array_channel1;
can_rx_model_data* can_rx_model_data_array_channel2;


// If an s-function needs initialisation the initialisation function is beeing called from the Outputs_wrapper when the model is beeing run the first time.
// After the first run the custom_s_functions_initialised variable is beeing set to true.
boolean_T custom_s_functions_initialized = false; 
boolean_T idleDrawn = false;

uint8_t u8x8_byte_4wire_hw_spi(u8x8_t *u8x8, uint8_t msg, uint8_t arg_int, void *arg_ptr);
uint8_t u8g2_gpio_and_delay_stm32(U8X8_UNUSED u8x8_t *u8x8, U8X8_UNUSED uint8_t msg, U8X8_UNUSED uint8_t arg_int, U8X8_UNUSED void *arg_ptr);

void draw_battery_icon(int posx, int posy, int sizex, int sizey, uint32_t soc);
void drawNavigation(int style);
// Is beeing called after every initialization function has been called
void after_initialization_step(){
	//ADC after_initializeation_step
	if(adc_average_block_count>0){
		/*adc1_samplearray.SampledValues = (uint32_t*) malloc(hadc1.Init.NbrOfConversion*ADC_SAMPLECOUNT*sizeof(uint32_t)*3);
		adc1_samplearray.samplecount = ADC_SAMPLECOUNT;
		adc1_samplearray.channelcount = hadc1.Init.NbrOfConversion;
		HAL_ADC_Start_DMA(&hadc1, adc1_samplearray.SampledValues, adc1_samplearray.channelcount*adc1_samplearray.samplecount);
*/
		
		// Achtung!!! L�nge ist Anzahl der Werte in etsprechender CubeMX Konfiguration.
		// Hier also 50 Werte vom Typ Half Word und damit 100 Byte.
		//HAL_ADC_Start_DMA(&hadc2, (uint32_t *)samplearray, 50);

		/*adc3_samplearray.SampledValues = (uint32_t*) malloc(hadc3.Init.NbrOfConversion*ADC_SAMPLECOUNT*sizeof(uint32_t));
		adc3_samplearray.samplecount = ADC_SAMPLECOUNT;
		adc3_samplearray.channelcount = hadc3.Init.NbrOfConversion;
		HAL_ADC_Start_DMA(&hadc3, adc3_samplearray.SampledValues, adc3_samplearray.channelcount*adc3_samplearray.samplecount);
*/
	}
	//CAN after_initializeation_step
	if(can_tx_block_count>0 || can1_rx_block_count+can2_rx_block_count>0){

		//filter can1
		sFilterConfig1.FilterNumber = 0;
		sFilterConfig1.FilterMode = CAN_FILTERMODE_IDMASK;
		sFilterConfig1.FilterScale = CAN_FILTERSCALE_16BIT;
		sFilterConfig1.FilterIdHigh = 0x0000;
		sFilterConfig1.FilterIdLow = 0x0000;
		sFilterConfig1.FilterMaskIdHigh = 0x0000;
		sFilterConfig1.FilterMaskIdLow = 0x0000;
		sFilterConfig1.FilterFIFOAssignment = CAN_FILTER_FIFO0;
		sFilterConfig1.FilterActivation = ENABLE;
		sFilterConfig1.BankNumber = 14;
		HAL_CAN_ConfigFilter(&hcan1, &sFilterConfig1);
		//filter can2
		sFilterConfig2.FilterNumber = 14;
		sFilterConfig2.FilterMode = CAN_FILTERMODE_IDMASK;
		sFilterConfig2.FilterScale = CAN_FILTERSCALE_16BIT;
		sFilterConfig2.FilterIdHigh = 0x0000;
		sFilterConfig2.FilterIdLow = 0x0000;
		sFilterConfig2.FilterMaskIdHigh = 0x0000;
		sFilterConfig2.FilterMaskIdLow = 0x0000;
		sFilterConfig2.FilterFIFOAssignment = CAN_FILTER_FIFO1;
		sFilterConfig2.FilterActivation = ENABLE;
		sFilterConfig2.BankNumber = 14;
		//HAL_CAN_ConfigFilter(&hcan2, &sFilterConfig2);

		//setup can massages
		hcan1.pTxMsg = &TxMessage1;
		hcan1.pRxMsg = &RxMessage1;
		//hcan2.pTxMsg = &TxMessage2;
		//hcan2.pRxMsg = &RxMessage2;

		//can tranceiver standby
		HAL_GPIO_WritePin(GPIOG,GPIO_PIN_9,0);//can1
		HAL_GPIO_WritePin(GPIOB,GPIO_PIN_7,0);//can2

		can_rx_model_data_array_channel1=malloc(sizeof(can_rx_model_data)*can1_rx_block_count);
		can_rx_model_data_array_channel2=malloc(sizeof(can_rx_model_data)*can2_rx_block_count);
		for(int i=0;i<can1_rx_block_count;i++){
			can_rx_model_data_array_channel1[i].received=false;
		}
		for(int i=0;i<can2_rx_block_count;i++){
			can_rx_model_data_array_channel2[i].received=false;
		}

		//start receive
		volatile HAL_StatusTypeDef res;
		res = HAL_CAN_Receive_IT(&hcan1,CAN_FIFO0);
		//res = HAL_CAN_Receive_IT(&hcan2,CAN_FIFO1);
	}

	if(oled_draw_block_count > 0){
		//init oled screen
		
		
		//u8g2_Setup_ssd1309_128x64_custom
		u8g2_Setup_ssd1309_128x64_custom(&u8g2, U8G2_R2, u8x8_byte_4wire_hw_spi, u8g2_gpio_and_delay_stm32); // setup internal buffers and structures
		u8g2_InitDisplay(&u8g2);     // transfer init sequence to the display
		
		HAL_SPI_DeInit(&hspi2);
		hspi2.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_8;
		if (HAL_SPI_Init(&hspi2) != HAL_OK)
		{
			_Error_Handler(__FILE__, __LINE__);
		}

		//u8g2_Setup_ssd1309_128x64_custom
		u8g2_Setup_ssd1309_128x64_custom(&u8g2, U8G2_R2, u8x8_byte_4wire_hw_spi, u8g2_gpio_and_delay_stm32); // setup internal buffers and structures
		u8g2_InitDisplay(&u8g2);     // transfer init sequence to the display
		u8g2_ClearDisplay(&u8g2);
		u8g2_SetPowerSave(&u8g2, 0); // turn on display
		u8g2_ClearBuffer(&u8g2);
		u8g2_SendBuffer(&u8g2);
		HAL_Delay(1);
		
		
	}
}

//GPIO IN

void gpio_in_Outputs_wrapper(const int32_T *port_popupvalue, const int32_T *pin_number, boolean_T *gpio_in, const real_T *xD){
	if(*port_popupvalue==1){
		*gpio_in=HAL_GPIO_ReadPin(GPIOA,(1<<*pin_number));
	}else if (*port_popupvalue==2){
		*gpio_in=HAL_GPIO_ReadPin(GPIOB,(1<<*pin_number));
	}else if (*port_popupvalue==3){
		*gpio_in=HAL_GPIO_ReadPin(GPIOC,(1<<*pin_number));
	}else if (*port_popupvalue==4){
		*gpio_in=HAL_GPIO_ReadPin(GPIOD,(1<<*pin_number));
	}else if (*port_popupvalue==5){
		*gpio_in=HAL_GPIO_ReadPin(GPIOE,(1<<*pin_number));
	}else if (*port_popupvalue==6){
		*gpio_in=HAL_GPIO_ReadPin(GPIOF,(1<<*pin_number));
	}else if (*port_popupvalue==7){
		*gpio_in=HAL_GPIO_ReadPin(GPIOG,(1<<*pin_number));
	}else if (*port_popupvalue==8){
		*gpio_in=HAL_GPIO_ReadPin(GPIOH,(1<<*pin_number));
	}else if (*port_popupvalue==9){
		*gpio_in=HAL_GPIO_ReadPin(GPIOI,(1<<*pin_number));
	}
}

void gpio_in_Update_wrapper(const int32_T *port_popupvalue,
                            const int32_T *pin_number,
                            boolean_T *gpio_in,
                            real_T *xD) {
	// No content
}

//GPIO OUT

void gpio_out_Outputs_wrapper(const int32_T *port_popupvalue, const int32_T *pin_number, const boolean_T *gpio_out, const real_T *xD){
	if(*port_popupvalue==1){
		HAL_GPIO_WritePin(GPIOA,(1<<*pin_number),*gpio_out);
	}else if (*port_popupvalue==2){
		HAL_GPIO_WritePin(GPIOB,(1<<*pin_number),*gpio_out);
	}else if (*port_popupvalue==3){
		HAL_GPIO_WritePin(GPIOC,(1<<*pin_number),*gpio_out);
	}else if (*port_popupvalue==4){
		HAL_GPIO_WritePin(GPIOD,(1<<*pin_number),*gpio_out);
	}else if (*port_popupvalue==5){
		HAL_GPIO_WritePin(GPIOE,(1<<*pin_number),*gpio_out);
	}else if (*port_popupvalue==6){
		HAL_GPIO_WritePin(GPIOF,(1<<*pin_number),*gpio_out);
	}else if (*port_popupvalue==7){
		HAL_GPIO_WritePin(GPIOG,(1<<*pin_number),*gpio_out);
	}else if (*port_popupvalue==8){
		HAL_GPIO_WritePin(GPIOH,(1<<*pin_number),*gpio_out);
	}else if (*port_popupvalue==9){
		HAL_GPIO_WritePin(GPIOI,(1<<*pin_number),*gpio_out);
	}
}

void gpio_out_Update_wrapper(const int32_T *port_popupvalue,
                             const int32_T *pin_number,
                             const boolean_T *gpio_out,
                             real_T *xD) {
	// No content
}

//ADC
//uint32_t get_ADC_Value(struct samplearray adc_samplearray, int channel_ranknumber){
// int i;
// uint32_t r = adc_samplearray.SampledValues[i];
// for(i = channel_ranknumber-1; i<adc_samplearray.channelcount*adc_samplearray.samplecount; i += adc_samplearray.channelcount){
//	r = (r + adc_samplearray.SampledValues[i]);
// }
// return r/adc_samplearray.samplecount;
//}

void adc_average_Initialize(){
	adc_average_block_count++;
}
void adc_average_Outputs_wrapper(const int32_T *rank,const int32_T *adc,uint32_T *adc_value,const real_T *xD){
	if(!custom_s_functions_initialized){
		adc_average_Initialize();
		return;
	}
//	if(*adc == 1){
//		*adc_value=get_ADC_Value(adc1_samplearray, *rank);
//	}
/*	if(*adc == 2){
		uint32_t value = 0; 
		for(unsigned int i = 0; i < 50; i+=2){
			value += samplearray[i];
		}
		value /= 25;
		*adc_value = value;
	}*/
//	if(*adc == 3){
//		*adc_value=get_ADC_Value(adc3_samplearray, *rank);
//	}

}
void adc_average_Update_wrapper(const int32_T *rank,const int32_T *adc,const uint32_T *adc_value,real_T *xD){
}

//DAC

void dac1_Initialize(){
//	HAL_DAC_Start(&hdac, DAC_CHANNEL_1);
}
void dac1_Outputs_wrapper(const uint32_T *dac_value,const real_T *xD){
	if(!custom_s_functions_initialized){
		dac1_Initialize();
	}
//	HAL_DAC_SetValue(&hdac, DAC_CHANNEL_1, DAC_ALIGN_12B_R, *dac_value);
}
void dac1_Update_wrapper(const uint32_T *dac_value,real_T *xD){
}

// CAN Tx

void can_tx_Initialize(){
	can_tx_block_count++;
}

void can_tx_Outputs_wrapper(const boolean_T *trigger,
  const uint32_T *id,
  const uint32_T *dlc,
  const uint8_T *data,
  const uint32_T *channel,
  real_T *xD){
	if(!custom_s_functions_initialized){
		can_tx_Initialize();
	}
	if(*trigger && custom_s_functions_initialized){
		if(*channel==1){
		hcan1.pTxMsg->StdId = *id;
		hcan1.pTxMsg->RTR = CAN_RTR_DATA;
		hcan1.pTxMsg->IDE = CAN_ID_STD;
		hcan1.pTxMsg->DLC = *dlc;
		hcan1.pTxMsg->Data[0] = data[0];
		hcan1.pTxMsg->Data[1] = data[1];
		hcan1.pTxMsg->Data[2] = data[2];
		hcan1.pTxMsg->Data[3] = data[3];
		hcan1.pTxMsg->Data[4] = data[4];
		hcan1.pTxMsg->Data[5] = data[5];
		hcan1.pTxMsg->Data[6] = data[6];
		hcan1.pTxMsg->Data[7] = data[7];
		//volatile HAL_StatusTypeDef res =
		volatile HAL_StatusTypeDef res = HAL_CAN_Transmit(&hcan1,1);
		}
		if(*channel==2){
		/*hcan2.pTxMsg->StdId = *id;
		hcan2.pTxMsg->RTR = CAN_RTR_DATA;
		hcan2.pTxMsg->IDE = CAN_ID_STD;
		hcan2.pTxMsg->DLC = *dlc;
		hcan2.pTxMsg->Data[0] = data[0];
		hcan2.pTxMsg->Data[1] = data[1];
		hcan2.pTxMsg->Data[2] = data[2];
		hcan2.pTxMsg->Data[3] = data[3];
		hcan2.pTxMsg->Data[4] = data[4];
		hcan2.pTxMsg->Data[5] = data[5];
		hcan2.pTxMsg->Data[6] = data[6];
		hcan2.pTxMsg->Data[7] = data[7];
		//volatile HAL_StatusTypeDef res =
		volatile HAL_StatusTypeDef res = HAL_CAN_Transmit(&hcan2,1);*/
		}
	}

}


void can_tx_Update_wrapper(const boolean_T *trigger,
  const uint32_T *id,
  const uint32_T *dlc,
  const uint8_T *data,
  const uint32_T *channel,
  real_T *xD){
}

//CAN Rx

void can_rx_Initialize(const uint32_T *id, const uint32_T *channel){
	if(can1_rx_block_count+can2_rx_block_count<MAX_CAN_RX_COUNT){
		if(*channel == 1){
			can_rx_ids_channel1[can1_rx_block_count]=*id;
			can1_rx_block_count++;
		}
		if(*channel == 2){
			can_rx_ids_channel2[can2_rx_block_count]=*id;
			can2_rx_block_count++;
		}
	}
}

void can_rx_Outputs_wrapper(const uint32_T *id,
  const uint32_T *channel,
  boolean_T *received,
  uint8_T *data,
  const real_T *xD){
	if(!custom_s_functions_initialized){
		can_rx_Initialize(id,channel);
	}
	else
	{
		if(*channel==1){
			unsigned int index;
			//find the id in the can_rx_ids array	with a linear search		
			for (index = 0; can_rx_ids_channel1[index] != *id; index += 1);

			data[0] = can_rx_model_data_array_channel1[index].data_0;
			data[1] = can_rx_model_data_array_channel1[index].data_1;
			data[2] = can_rx_model_data_array_channel1[index].data_2;
			data[3] = can_rx_model_data_array_channel1[index].data_3;
			data[4] = can_rx_model_data_array_channel1[index].data_4;
			data[5] = can_rx_model_data_array_channel1[index].data_5;
			data[6] = can_rx_model_data_array_channel1[index].data_6;
			data[7] = can_rx_model_data_array_channel1[index].data_7;

			*received = can_rx_model_data_array_channel1[index].received;

			can_rx_model_data_array_channel1[index].received = false;
		}
		if(*channel==2){
			int index = 0;
			//find the id in the can_rx_ids array
			while(can_rx_ids_channel2[index] != *id){
				index++;
			}

			data[0] = can_rx_model_data_array_channel2[index].data_0;
			data[1] = can_rx_model_data_array_channel2[index].data_1;
			data[2] = can_rx_model_data_array_channel2[index].data_2;
			data[3] = can_rx_model_data_array_channel2[index].data_3;
			data[4] = can_rx_model_data_array_channel2[index].data_4;
			data[5] = can_rx_model_data_array_channel2[index].data_5;
			data[6] = can_rx_model_data_array_channel2[index].data_6;
			data[7] = can_rx_model_data_array_channel2[index].data_7;

			*received = can_rx_model_data_array_channel2[index].received;

			can_rx_model_data_array_channel2[index].received = false;
		}
	}
}

void can_rx_Update_wrapper(const boolean_T *trigger,
  const uint32_T *id,
  const uint32_T *dlc,
  const uint8_T *data,
  const uint32_T *channel,
  const real_T *xD){

}

void HAL_CAN_RxCpltCallback(CAN_HandleTypeDef * hcan){
//	can_rx_queue_data queue_data_struct;
//	queue_data_struct.id = hcan->pRxMsg->StdId;
//	queue_data_struct.dlc = hcan->pRxMsg->DLC;
//	queue_data_struct.data_0 = hcan->pRxMsg->Data[0];
//	queue_data_struct.data_1 = hcan->pRxMsg->Data[1];
//	queue_data_struct.data_2 = hcan->pRxMsg->Data[2];
//	queue_data_struct.data_3 = hcan->pRxMsg->Data[3];
//	queue_data_struct.data_4 = hcan->pRxMsg->Data[4];
//	queue_data_struct.data_5 = hcan->pRxMsg->Data[5];
//	queue_data_struct.data_6 = hcan->pRxMsg->Data[6];
//	queue_data_struct.data_7 = hcan->pRxMsg->Data[7];
//	xQueueSendFromISR(can_rx_queueHandle, &queue_data_struct, NULL);
//	HAL_CAN_Receive_IT(&hcan1,CAN_FIFO0);
	if(custom_s_functions_initialized){

		if(hcan == &hcan1){
			for(int index=0;index<can1_rx_block_count;index++){
				//check if the recieved massage has an entry in the can_rx_ids array
				if (hcan->pRxMsg->StdId == can_rx_ids_channel1[index]){
					//write the data of the massage into the can_rx_data_array
					can_rx_model_data_array_channel1[index].data_0 = hcan->pRxMsg->Data[0];
					can_rx_model_data_array_channel1[index].data_1 = hcan->pRxMsg->Data[1];
					can_rx_model_data_array_channel1[index].data_2 = hcan->pRxMsg->Data[2];
					can_rx_model_data_array_channel1[index].data_3 = hcan->pRxMsg->Data[3];
					can_rx_model_data_array_channel1[index].data_4 = hcan->pRxMsg->Data[4];
					can_rx_model_data_array_channel1[index].data_5 = hcan->pRxMsg->Data[5];
					can_rx_model_data_array_channel1[index].data_6 = hcan->pRxMsg->Data[6];
					can_rx_model_data_array_channel1[index].data_7 = hcan->pRxMsg->Data[7];
					can_rx_model_data_array_channel1[index].received = true;
					break;
				}
			}
		}
		/*if(hcan == &hcan2){
			for(int index=0;index<can2_rx_block_count;index++){
				//check if the recieved massage has an entry in the can_rx_ids array
				if (hcan->pRxMsg->StdId == can_rx_ids_channel2[index]){
					//write the data of the massage into the can_rx_data_array
					can_rx_model_data_array_channel2[index].data_0 = hcan->pRxMsg->Data[0];
					can_rx_model_data_array_channel2[index].data_1 = hcan->pRxMsg->Data[1];
					can_rx_model_data_array_channel2[index].data_2 = hcan->pRxMsg->Data[2];
					can_rx_model_data_array_channel2[index].data_3 = hcan->pRxMsg->Data[3];
					can_rx_model_data_array_channel2[index].data_4 = hcan->pRxMsg->Data[4];
					can_rx_model_data_array_channel2[index].data_5 = hcan->pRxMsg->Data[5];
					can_rx_model_data_array_channel2[index].data_6 = hcan->pRxMsg->Data[6];
					can_rx_model_data_array_channel2[index].data_7 = hcan->pRxMsg->Data[7];
					can_rx_model_data_array_channel2[index].received = true;
					break;
				}
			}
		}*/

	}

	//force Receive_IT without messing with HAL_CAN_StateTypeDef

	/* Enable Error warning Interrupt */
	__HAL_CAN_ENABLE_IT(hcan, CAN_IT_EWG);

	/* Enable Error passive Interrupt */
	__HAL_CAN_ENABLE_IT(hcan, CAN_IT_EPV);

	/* Enable Bus-off Interrupt */
	__HAL_CAN_ENABLE_IT(hcan, CAN_IT_BOF);

	/* Enable Last error code Interrupt */
	__HAL_CAN_ENABLE_IT(hcan, CAN_IT_LEC);

	/* Enable Error Interrupt */
	__HAL_CAN_ENABLE_IT(hcan, CAN_IT_ERR);

	/* Process unlocked */
	__HAL_UNLOCK(hcan);


    if(hcan == &hcan1)//can1 frames to fifo0
    {
      /* Enable FIFO 0 message pending Interrupt */
      __HAL_CAN_ENABLE_IT(hcan, CAN_IT_FMP0);
    }
/*    if(hcan == &hcan2)//can2 frames to fifo0
    {
       Enable FIFO 1 message pending Interrupt 
      __HAL_CAN_ENABLE_IT(hcan, CAN_IT_FMP1);
    }*/

}



// Input capture timer for flow-meters
// Counts between two capture events that correlates with the input frequency
uint32_T u32_CH3_Gap = 0;
uint32_T u32_CH4_Gap = 0;
// A counter wrap occurred between two capture events
boolean_T b_CH3_PeriodBeforeCapture = false;
boolean_T b_CH4_PeriodBeforeCapture = false;
// A double counter wrap occurred and the channel is defined as overflowed
boolean_T b_CH3_Overflow = false;
boolean_T b_CH4_Overflow = false;

// S-function
void Capture_Timer_Outputs_wrapper(const uint8_T *CH, uint32_T *Gap) {
	// Switch channel
	// Output gap value
	if(3 == *CH)
		*Gap = u32_CH3_Gap;
	else if(4 == *CH)
		*Gap = u32_CH4_Gap;
}

void Capture_Timer_Update_wrapper(const uint8_T *CH, uint32_T *Gap, real_T *xD) {
	// No content
}

// Callbacks
// HAL_TIM_PeriodElapsedCallback is defined in main.c

void HAL_TIM_IC_CaptureCallback (TIM_HandleTypeDef * htim) {
	// The previous capture value
	static uint32_T u32_CH3_LastCapture = 0;
	static uint32_T u32_CH4_LastCapture = 0;
  // Switch timer instance
/*  if(htim == &htim4) {
  	if(htim->Channel == HAL_TIM_ACTIVE_CHANNEL_3) {
  		// Capture event on channel 3
  		// Calculate gap value
  		if(b_CH3_Overflow == true) {
  			// No valid last capture value available due to channel overflow
  			// Start new cycle by waiting for a second capture event with valid last capture value
  			b_CH3_Overflow = false;
  			b_CH3_PeriodBeforeCapture = false;
  			u32_CH3_LastCapture = HAL_TIM_ReadCapturedValue(htim, TIM_CHANNEL_3);
  			// Gap value has already been set to 0 in HAL_TIM_PeriodElapsedCallback()
  		}	else if(b_CH3_PeriodBeforeCapture == true) {
  			// Consider counter wrap
  			u32_CH3_Gap = (0xFFFFFFFF - u32_CH3_LastCapture) + HAL_TIM_ReadCapturedValue(htim, TIM_CHANNEL_3);
  			b_CH3_PeriodBeforeCapture = false;
			}	else u32_CH3_Gap = HAL_TIM_ReadCapturedValue(htim, TIM_CHANNEL_3) - u32_CH3_LastCapture;
  	} else if(htim->Channel == HAL_TIM_ACTIVE_CHANNEL_4) {
  		// Capture event on channel 4
  		// Calculate gap value
  		if(b_CH4_Overflow == true) {
  			// No valid last capture value available due to channel overflow
  			// Start new cycle by waiting for a second capture event with valid last capture value
  			b_CH4_Overflow = false;
  			b_CH4_PeriodBeforeCapture = false;
  			u32_CH4_LastCapture = HAL_TIM_ReadCapturedValue(htim, TIM_CHANNEL_4);
  		} else if(b_CH4_PeriodBeforeCapture == true) {
  			// Consider counter wrap
  			u32_CH4_Gap = (0xFFFFFFFF - u32_CH4_LastCapture) + HAL_TIM_ReadCapturedValue(htim, TIM_CHANNEL_4);
  			b_CH4_PeriodBeforeCapture = false;
			}	else u32_CH4_Gap = HAL_TIM_ReadCapturedValue(htim, TIM_CHANNEL_4) - u32_CH4_LastCapture;
  	}
  }*/
}


//OLED Draw initialize
void oled_draw_Initialize(){
	oled_draw_block_count ++;
}

//is used to redraw the oled screen
//only gets called if menu point changes, value in-/decreased or new value is recieved via CAN
void oled_draw_Outputs_wrapper(const boolean_T *draw, const real_T *xD){
	if(!custom_s_functions_initialized){
		oled_draw_Initialize();
		return;
	}
	
	if(*draw == true){
		
		//HAL_CAN_MspDeInit(&hcan1);
		HAL_NVIC_DisableIRQ(CAN1_RX0_IRQn);
    HAL_NVIC_DisableIRQ(CAN1_RX1_IRQn);
    HAL_NVIC_DisableIRQ(CAN1_SCE_IRQn);
		
		u8g2_SendBuffer(&u8g2);	

		HAL_CAN_MspInit(&hcan1);
		
	}
}
void oled_draw_Update_wrapper(const boolean_T *draw, const real_T *xD){}

void oled_sdc_Outputs_wrapper(const uint32_T *sdc_status,
			const boolean_T *show,
			const real_T *xD)
{
	if(oled_draw_block_count > 0 && *show){
		u8g2_ClearBuffer(&u8g2);		
		
		drawNavigation(NEUTRAL);
		u8g2_SetFont(&u8g2, u8g2_font_9x15_tf);
    u8g2_DrawStr(&u8g2, 49,15,"SDC");
		char sdc_status_string[30];

		switch(*sdc_status){
				default: 
				u8g2_DrawStr(&u8g2, 44,30,"OPEN");
				snprintf(sdc_status_string, 30, "ERROR");
					break;
				case 0: 
				u8g2_DrawStr(&u8g2, 44,30,"OPEN");	
				snprintf(sdc_status_string, 30, "INERTIA ...");
					break;
				case 1: 
				u8g2_DrawStr(&u8g2, 44,30,"OPEN");	
				snprintf(sdc_status_string, 30, "SDB COCKPIT ...");
					break;
				case 3: 
				u8g2_DrawStr(&u8g2, 44,30,"OPEN");	
				snprintf(sdc_status_string, 30, "MOTOR FRONT ...");
					break;
				case 7: 
				u8g2_DrawStr(&u8g2, 44,30,"OPEN");	
				snprintf(sdc_status_string, 30, "BOTS ...");
					break;
				case 15: 
				u8g2_DrawStr(&u8g2, 44,30,"OPEN");	
				snprintf(sdc_status_string, 30, "SDB HOOP ...");
					break;
				case 31: 
				u8g2_DrawStr(&u8g2, 44,30,"OPEN");	
				snprintf(sdc_status_string, 30, "BSPD ...");
					break;
				case 63: 
				u8g2_DrawStr(&u8g2, 44,30,"OPEN");	
				snprintf(sdc_status_string, 30, "LE BOX ...");
					break;
				case 127: 
				u8g2_DrawStr(&u8g2, 44,30,"OPEN");	
				snprintf(sdc_status_string, 30, "HVD ...");
					break;
				case 255: 
				u8g2_DrawStr(&u8g2, 44,30,"OPEN");	
				snprintf(sdc_status_string, 30, "MOTOR REAR ...");
					break;
				case 511:
					u8g2_DrawRBox(&u8g2, 50, 24, 24, 14, 3);
					u8g2_SetDrawColor(&u8g2, 0);
					u8g2_DrawStr(&u8g2, 53,36,"OK");
					u8g2_SetDrawColor(&u8g2, 1);
					break;

		}
		if(*sdc_status != 511){
			u8g2_SetFont(&u8g2, u8g2_font_5x7_tf);
			u8g2_DrawStr(&u8g2, 2,60, sdc_status_string);
		}
		
	}
}

void oled_sdc_Update_wrapper(const uint32_T *sdc_status,
			const boolean_T *show,
			real_T *xD)
{}

void oled_rtd_Outputs_wrapper(const boolean_T *show,
			const real_T *xD)
{
	if(oled_draw_block_count > 0 && *show){#
			u8g2_ClearBuffer(&u8g2);

			u8g2_SetFont(&u8g2, u8g2_font_helvR24_tf);
			u8g2_DrawStr(&u8g2, 20, 45, "RTD");
	}
}

void oled_rtd_Update_wrapper(const boolean_T *show,
			real_T *xD)
{}


void oled_lv_Outputs_wrapper(const real_T *lv_voltage,
			const uint32_T *lv_soc,
			const boolean_T *show,
			const real_T *xD)
{
	if(oled_draw_block_count > 0 && *show){
		u8g2_ClearBuffer(&u8g2);
		
		drawNavigation(LEFT);
		u8g2_SetFont(&u8g2, u8g2_font_9x15_tf);
    u8g2_DrawStr(&u8g2, 52,15,"LV");
		draw_battery_icon(37, 20, 55, 20, *lv_soc);
		
		
		char lv_voltage_string[7];
		snprintf(lv_voltage_string, 7, "%.2fV", *lv_voltage);
		u8g2_DrawStr(&u8g2, 35,55, lv_voltage_string);
	}
}

void oled_lv_Update_wrapper(const real_T *lv_voltage,
			const uint32_T *lv_soc,
			const boolean_T *show,
			real_T *xD)
{}

void oled_idle_Outputs_wrapper(const boolean_T *show,
			const real_T *xD)
{
	if(oled_draw_block_count > 0 && *show){
			
		if(idleDrawn == false){
			u8g2_ClearBuffer(&u8g2);
			drawNavigation(LEFT);
			u8g2_DrawXBMP(&u8g2, 38, 2, starkstrom_logo_width, starkstrom_logo_height, starkstrom_logo_bits);
			idleDrawn = true;
		}
	}
}

void oled_idle_Update_wrapper(const boolean_T *show,
			real_T *xD)
{}

void oled_hv_Outputs_wrapper(const uint32_T *hv_soc,
			const real_T *cell_max,
			const real_T *cell_min,
			const real_T *temp_max,
			const real_T *temp_min,
			const boolean_T *show,
			const real_T *xD)
{
	if(oled_draw_block_count > 0 && *show){
			u8g2_ClearBuffer(&u8g2);
		
			drawNavigation(NEUTRAL);
			u8g2_SetFont(&u8g2, u8g2_font_9x15_tf);
			u8g2_DrawStr(&u8g2, 52,15,"HV");
			draw_battery_icon(37, 20, 55, 20, *hv_soc);
		
			char hv_voltage_string[30];
			char hv_temp_string[30];
			snprintf(hv_voltage_string, 30, "MAX:%.2fV MIN:%.2fV", *cell_max, *cell_min);
			snprintf(hv_temp_string, 30, "TEMP:%.0fC", *temp_max);
			u8g2_SetFont(&u8g2, u8g2_font_5x7_tf);
			u8g2_DrawStr(&u8g2, 20, 53, hv_voltage_string);
			u8g2_DrawStr(&u8g2, 20, 62, hv_temp_string);
			
	}
}

void oled_hv_Update_wrapper(const uint32_T *hv_soc,
			const real_T *cell_max,
			const real_T *cell_min,
			const real_T *temp_max,
			const real_T *temp_min,
			const boolean_T *show,
			real_T *xD)
{}

void oled_driving_Outputs_wrapper(const real_T *power,
			const uint32_T *hv_soc,
			const uint32_T *lv_soc,
			const uint32_T *error,
			const real_T *cell_min,
			const uint32_T *accu_temp,
			const boolean_T *show,
			const real_T *xD)
{
	if(oled_draw_block_count > 0 && *show){
			u8g2_ClearBuffer(&u8g2);

			//u8g2_SetFont(&u8g2, u8g2_font_helvR24_tf);
			//u8g2_DrawStr(&u8g2, 20, 45, "RTD");
	}
}

void oled_driving_Update_wrapper(const real_T *power,
			const uint32_T *hv_soc,
			const uint32_T *lv_soc,
			const uint32_T *error,
			const real_T *cell_min,
			const uint32_T *accu_temp,
			const boolean_T *show,
			real_T *xD)
{}

void oled_cooling_Outputs_wrapper(const uint32_T *fan,
			const real_T *temp_max,
			const boolean_T *show,
			const real_T *xD)
{
	if(oled_draw_block_count > 0 && *show){
		u8g2_ClearBuffer(&u8g2);
		
		drawNavigation(NEUTRAL);
		u8g2_SetFont(&u8g2, u8g2_font_9x15_tf);
		u8g2_DrawStr(&u8g2, 40,15,"COOLING");
	}
}

void oled_cooling_Update_wrapper(const uint32_T *fan,
			const real_T *temp_max,
			const boolean_T *show,
			real_T *xD)
{}

void oled_asr_Outputs_wrapper(const boolean_T *show,
			const boolean_T *selected,
			const uint8_T *intensity,
			const uint8_T *status,
			const real_T *xD)
{
	if(oled_draw_block_count > 0 && *show){
		u8g2_ClearBuffer(&u8g2);
		
		drawNavigation(RIGHT);
		
		u8g2_SetFont(&u8g2, u8g2_font_9x15_tf);

		if(*selected){
			u8g2_DrawRBox(&u8g2, 50, 24, 31, 14, 3);
			u8g2_SetDrawColor(&u8g2, 0);
			u8g2_DrawStr(&u8g2, 53,36,"ASR");
			u8g2_SetDrawColor(&u8g2, 1);
		}
		else{
			u8g2_SetDrawColor(&u8g2, 1);
			u8g2_SetFont(&u8g2, u8g2_font_9x15_tf);
			u8g2_DrawStr(&u8g2, 48,15,"ASR");
		}
		
		char asr_intenstity_string[5];
			snprintf(asr_intenstity_string, 5, "%.0d%%", *intensity);
			u8g2_DrawStr(&u8g2, 43, 40, hv_voltage_string);

		u8g2_DrawStr(&u8g2, 24,56,"OFF");
		u8g2_DrawStr(&u8g2, 59,56,"DRY");
		u8g2_DrawStr(&u8g2, 94,56,"WET");

		switch(*status){
			case 0:
				u8g2_SetDrawColor(&u8g2, 1);
				u8g2_DrawRBox(&u8g2, 22, 44, 31, 14, 2);
				u8g2_SetDrawColor(&u8g2, 0);
				u8g2_DrawStr(&u8g2, 24,56,"OFF");
				break;
			case 1:
				u8g2_SetDrawColor(&u8g2, 1);
				u8g2_DrawRBox(&u8g2, 57, 44, 31, 14, 2);
				u8g2_SetDrawColor(&u8g2, 0);
				u8g2_DrawStr(&u8g2, 24,56,"DRY");
				break;
			case 2: 
				u8g2_SetDrawColor(&u8g2, 1);
				u8g2_DrawRBox(&u8g2, 92, 44, 31, 14, 2);
				u8g2_SetDrawColor(&u8g2, 0);
				u8g2_DrawStr(&u8g2, 24,56,"WET");
				break;
		}
	}
}


void oled_asr_Update_wrapper(const boolean_T *show,
			const boolean_T *selected,
			const uint8_T *intensity,
			const uint8_T *status,
			real_T *xD)
{
}

//Hardware SPI2 configuration
uint8_t u8x8_byte_4wire_hw_spi(u8x8_t *u8x8, uint8_t msg, uint8_t arg_int, void *arg_ptr)
{
  uint8_t byte;
  uint8_t *data;
 
  switch(msg){
    case U8X8_MSG_BYTE_SEND:
      data = (uint8_t *)arg_ptr;
      while( arg_int > 0 )
      {
        byte = *data;
        data++;
        arg_int--;

				HAL_SPI_Transmit_DMA(&hspi2, &byte, 1);
        //HAL_SPI_Transmit(&hspi2, &byte, 1, 100);
      }
      break;
    case U8X8_MSG_BYTE_START_TRANSFER:
      HAL_GPIO_WritePin(GPIOB, SPI2_CS_Pin, GPIO_PIN_RESET);
      //__nop(); // 21 ns
      break;
    case U8X8_MSG_BYTE_END_TRANSFER:
      //__nop(); // 21 ns
      HAL_GPIO_WritePin(GPIOB, SPI2_CS_Pin, GPIO_PIN_SET);
      break;
		//Function to define the logic level of the Data/ Command line
		case U8X8_MSG_BYTE_SET_DC:
			if (arg_int) HAL_GPIO_WritePin(GPIOC, SPI2_DC_Pin, GPIO_PIN_SET);
			else HAL_GPIO_WritePin(GPIOC, SPI2_DC_Pin, GPIO_PIN_RESET);
			break;
		default:
			return 0;
  }
  return 1;

}


//custom delay function for SPI communication
uint8_t u8g2_gpio_and_delay_stm32(U8X8_UNUSED u8x8_t *u8x8, U8X8_UNUSED uint8_t msg, U8X8_UNUSED uint8_t arg_int, U8X8_UNUSED void *arg_ptr){
	switch(msg){
		//Function to define the logic level of the RESET line
		case U8X8_MSG_GPIO_RESET:
			HAL_GPIO_WritePin(GPIOC, OLED_RST_N_Pin, GPIO_PIN_RESET);
			//10us	
			for (uint16_t n = 0; n < 1000; n++)
			{
				__NOP();
			}
			HAL_GPIO_WritePin(GPIOC, OLED_RST_N_Pin, GPIO_PIN_SET);
			__nop(); // 21 ns
		break;
		default:
			return 0;
	}
	return 1; // command processed successfully.
}


//input of soc is 0 to 100
void draw_battery_icon(int posx, int posy, int sizex, int sizey, uint32_t soc){
		int size_pole_x = sizex / 10;
		int size_pole_y = sizey / 2;
		int size_body_x = (sizex * 0.9);
		int size_body_y = sizey;
		u8g2_DrawLine(&u8g2, posx + size_pole_x, posy, posx + size_body_x, posy);
		u8g2_DrawLine(&u8g2, posx + size_body_x, posy, posx + size_body_x, posy + size_body_y);
		u8g2_DrawLine(&u8g2, posx + size_body_x, posy + size_body_y, posx + size_pole_x, posy + size_body_y);
		u8g2_DrawLine(&u8g2, posx + size_pole_x, posy + size_body_y, posx + size_pole_x, posy + size_body_y - (0.5*(size_body_y-size_pole_y)));
		u8g2_DrawLine(&u8g2, posx + size_pole_x, posy + size_body_y - (0.5*(size_body_y-size_pole_y)),posx, posy + size_body_y - (0.5*(size_body_y-size_pole_y)) );
		u8g2_DrawLine(&u8g2, posx, posy + size_body_y - (0.5*(size_body_y-size_pole_y)), posx, posy + (0.5*(size_body_y-size_pole_y)) );
		u8g2_DrawLine(&u8g2, posx, posy + (0.5*(size_body_y-size_pole_y)), posx + size_pole_x, posy + (0.5*(size_body_y-size_pole_y)));
		u8g2_DrawLine(&u8g2, posx + size_pole_x, posy + (0.5*(size_body_y-size_pole_y)), posx + size_pole_x, posy);

		if(soc>100){
			soc = 100;
		}
		int soc_body;
		int soc_pole;
		if(soc <= 90){
			soc_body = (soc / 90.0)*100;
			soc_pole = 0;
		}
		if(soc > 90){
			soc_body = 100;
			soc_pole = (soc - 90)*10;
		}
		
		u8g2_DrawBox(&u8g2, posx + size_pole_x + (size_body_x - 4) - (soc_body/100.0 * (size_body_x - 4)), posy, ((soc_body/100.0)* (size_body_x - 4)), size_body_y);
		u8g2_DrawBox(&u8g2, posx + size_pole_x - ((soc_pole)/100.0 * size_pole_x), posy + (0.5*(size_body_y-size_pole_y)), ((soc_pole)/100.0 * (size_pole_x + 2)), size_pole_y);
}

void drawNavigation(int style){
		u8g2_SetFont(&u8g2, u8g2_font_siji_t_6x10);    
	switch(style){
		case LEFT:
			u8g2_DrawGlyph(&u8g2, 116, 35, 0xe1a0 + 9);
			break;
		case RIGHT:
			u8g2_DrawGlyph(&u8g2, 0, 35, 0xe1a0 + 11);
			break;
		case NEUTRAL:
			u8g2_DrawGlyph(&u8g2, 0, 35, 0xe1a0 + 11);
			u8g2_DrawGlyph(&u8g2, 116, 35, 0xe1a0 + 9);
			break;
	}
		
}
