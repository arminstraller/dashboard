#include "model.h"

#include "FreeRTOS.h"
//#include "adc.h"
#include "can.h"
#include "spi.h"

#define MAX_CAN_RX_COUNT 500

typedef struct can_rx_model_data{
	uint8_T data_0;
	uint8_T data_1;
	uint8_T data_2;
	uint8_T data_3;
	uint8_T data_4;
	uint8_T data_5;
	uint8_T data_6;
	uint8_T data_7;
	boolean_T received;
}can_rx_model_data;

extern can_rx_model_data* can_rx_model_data_array;